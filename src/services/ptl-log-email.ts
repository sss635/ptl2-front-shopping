import type PtlLogEmail from '@/types/PtlLogEmail'
import http from './axios'

function getAllPtlLogEmails() {
  return http.get(`${import.meta.env.VITE_PTL_LOG_EMAIL}`)
}

function getOnePtlLogEmail(id: number) {
  return http.get(`${import.meta.env.VITE_PTL_LOG_EMAIL}/${id}`)
}

function savePtlLogEmail(ptlLogEmail: PtlLogEmail) {
  return http.post(`${import.meta.env.VITE_PTL_LOG_EMAIL}`, ptlLogEmail)
}

export default { getAllPtlLogEmails, getOnePtlLogEmail, savePtlLogEmail }
