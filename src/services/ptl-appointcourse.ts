import type PtlAppointCourseTeacher from '@/types/PtlAppointCourseTeacher'
import http from './axios'

function getAllPtlAppointCourseTeachers() {
  return http.get(`${import.meta.env.VITE_PTL_APPOINT_COU_TCH}`)
}

function getOnePtlAppointCourseTeacher(appl_id: number, psn_id: string) {
  return http.get(`${import.meta.env.VITE_PTL_APPOINT_COU_TCH}/${appl_id}/${psn_id}`)
}

function savePtlAppointCourseTeacher(
  appl_id: number,
  ptlAppointCourseTeacher: PtlAppointCourseTeacher
) {
  return http.post(
    `${import.meta.env.VITE_PTL_APPOINT_COU_TCH}/${appl_id}`,
    ptlAppointCourseTeacher
  )
}

function updatePtlAppointCourseTeacher(
  appl_id: number,
  psn_id: string,
  ptlAppointCourseTeacher: PtlAppointCourseTeacher
) {
  return http.patch(
    `${import.meta.env.VITE_PTL_APPOINT_COU_TCH}/${appl_id}/${psn_id}`,
    ptlAppointCourseTeacher
  )
}

function deletePtlAppointCourseTeacher(
  appl_id: number,
  psn_id: string,
  ptlAppointCourseTeacher: PtlAppointCourseTeacher
) {
  //send data for input DELETE_USER
  return http.delete(`${import.meta.env.VITE_PTL_APPOINT_COU_TCH}/${appl_id}/${psn_id}`, {
    data: ptlAppointCourseTeacher
  })
}

export default {
  getAllPtlAppointCourseTeachers,
  getOnePtlAppointCourseTeacher,
  savePtlAppointCourseTeacher,
  updatePtlAppointCourseTeacher,
  deletePtlAppointCourseTeacher
}
