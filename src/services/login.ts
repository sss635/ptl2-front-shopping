import http from './axios'

function refreshToken() {
  return http.post(`${import.meta.env.VITE_AUTHEN}/refreshToken`)
}

function loginBuu(username: string, password: string) {
  const dataReqDto = {
    username: username,
    password: password
  }
  return http.post(`${import.meta.env.VITE_SERVICE_API_V1_LOGIN_BUU}`, dataReqDto)
}

function getPersonFromCitizenID(citizenid: string) {
  const dataReqDto = {
    citizenid: citizenid
  }
  return http.post(`${import.meta.env.VITE_SERVICE_API_V1_GET_PERSON_FROM_CITIZENID}`, dataReqDto)
}

export default {
  loginBuu,
  getPersonFromCitizenID,
  refreshToken
}
