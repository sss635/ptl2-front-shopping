import type PtlLecturer from '@/types/PtlLecturer'
import http from './axios'

function getAllPtlLecturers() {
  return http.get(`${import.meta.env.VITE_PTL_LECTURER}`)
}

function getAllPtlLecturersByManP(take: number, page: number, keyword: string) {
  return http.get(
    `${import.meta.env.VITE_PTL_LECTURER}/manP?take=${take}&page=${page}&keyword=${keyword}`
  )
}

function getAllPtlLecturersByCF() {
  return http.get(`${import.meta.env.VITE_PTL_LECTURER}/CF`)
}

function getOnePtlLecturer(id: number) {
  return http.get(`${import.meta.env.VITE_PTL_LECTURER}/${id}`)
}

function getOnePtlLecturerByCid(cid: string) {
  return http.get(`${import.meta.env.VITE_PTL_LECTURER}/cid/${cid}`)
}

function checkLecContract(data: string) {
  const dataReqDto = {
    data: data
  }
  return http.post(`${import.meta.env.VITE_PTL_LECTURER}/api-check-lec-contract`, dataReqDto)
}

function savePtlLecturer(ptlLecturer: PtlLecturer) {
  return http.post(`${import.meta.env.VITE_PTL_LECTURER}`, ptlLecturer)
}

function updatePtlLecturer(id: number, ptlLecturer: PtlLecturer) {
  return http.patch(`${import.meta.env.VITE_PTL_LECTURER}/${id}`, ptlLecturer)
}

function updatePtlLecturerFromEmail(id: number, ptlLecturer: PtlLecturer) {
  return http.patch(`${import.meta.env.VITE_PTL_LECTURER}/from-email/${id}`, ptlLecturer)
}

function updatePtlLecturerCreateUser(id: number, ptlLecturer: PtlLecturer) {
  return http.patch(`${import.meta.env.VITE_PTL_LECTURER}/create-user/${id}`, ptlLecturer)
}

function deletePtlLecturer(id: number, ptlLecturer: PtlLecturer) {
  //send data for input DELETE_USER
  return http.delete(`${import.meta.env.VITE_PTL_LECTURER}/${id}`, { data: ptlLecturer })
}

export default {
  getAllPtlLecturers,
  getOnePtlLecturer,
  savePtlLecturer,
  updatePtlLecturer,
  deletePtlLecturer,
  getAllPtlLecturersByCF,
  getAllPtlLecturersByManP,
  updatePtlLecturerFromEmail,
  updatePtlLecturerCreateUser,
  getOnePtlLecturerByCid,
  checkLecContract
}
