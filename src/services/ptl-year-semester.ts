import type PtlYearSemester from '@/types/PtlYearSemester'
import http from './axios'

function getAllPtlYearSemesters() {
  return http.get(`${import.meta.env.VITE_PTL_YEAR_SEMESTER}`)
}

function getAllPtlYearSemestersByManP(take: number, page: number, keyword: string) {
  return http.get(
    `${import.meta.env.VITE_PTL_YEAR_SEMESTER}/manP?take=${take}&page=${page}&keyword=${keyword}`
  )
}

function getOnePtlYearSemester(id: number) {
  return http.get(`${import.meta.env.VITE_PTL_YEAR_SEMESTER}/${id}`)
}

function savePtlYearSemester(ptlYearSemester: PtlYearSemester) {
  return http.post(`${import.meta.env.VITE_PTL_YEAR_SEMESTER}`, ptlYearSemester)
}

function updatePtlYearSemester(id: number, ptlYearSemester: PtlYearSemester) {
  return http.patch(`${import.meta.env.VITE_PTL_YEAR_SEMESTER}/${id}`, ptlYearSemester)
}

function deletePtlYearSemester(id: number, ptlYearSemester: PtlYearSemester) {
  //send data for input DELETE_USER
  return http.delete(`${import.meta.env.VITE_PTL_YEAR_SEMESTER}/${id}`, { data: ptlYearSemester })
}

export default {
  getAllPtlYearSemesters,
  getOnePtlYearSemester,
  savePtlYearSemester,
  updatePtlYearSemester,
  deletePtlYearSemester,
  getAllPtlYearSemestersByManP
}
