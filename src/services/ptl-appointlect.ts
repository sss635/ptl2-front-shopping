import type PtlAppointLecturer from '@/types/PtlAppointLecturer'
import http from './axios'

function getAllPtlAppointLecturers() {
  return http.get(`${import.meta.env.VITE_PTL_APPOINT_LECT}`)
}

function getOnePtlAppointLecturer(id: number) {
  return http.get(`${import.meta.env.VITE_PTL_APPOINT_LECT}/${id}`)
}

function getOnePtlAppointLecturerCheck(app_id: number, ptlAppointLecturer: PtlAppointLecturer) {
  return http.post(`${import.meta.env.VITE_PTL_APPOINT_LECT}/check/${app_id}`, ptlAppointLecturer)
}

function savePtlAppointLecturer(app_id: number, ptlAppointLecturer: PtlAppointLecturer) {
  return http.post(`${import.meta.env.VITE_PTL_APPOINT_LECT}/${app_id}`, ptlAppointLecturer)
}

function updatePtlAppointLecturer(id: number, ptlAppointLecturer: PtlAppointLecturer) {
  return http.patch(`${import.meta.env.VITE_PTL_APPOINT_LECT}/${id}`, ptlAppointLecturer)
}

function deletePtlAppointLecturer(id: number, ptlAppointLecturer: PtlAppointLecturer) {
  //send data for input DELETE_USER
  return http.delete(`${import.meta.env.VITE_PTL_APPOINT_LECT}/${id}`, { data: ptlAppointLecturer })
}

export default {
  getAllPtlAppointLecturers,
  getOnePtlAppointLecturer,
  savePtlAppointLecturer,
  updatePtlAppointLecturer,
  deletePtlAppointLecturer,
  getOnePtlAppointLecturerCheck
}
