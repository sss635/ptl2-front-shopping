import http from './axios'

function getAdminViewAppoint(take: number, page: number, keyword: string) {
  return http.get(
    `${import.meta.env.VITE_QUERY}/admin-view-appoint?take=${take}&page=${page}&keyword=${keyword}`
  )
}

function getAdminViewAppointByFac(fac_id: string, take: number, page: number, keyword: string) {
  return http.get(
    `${
      import.meta.env.VITE_QUERY
    }/admin-view-appoint-fac?fac_id=${fac_id}&take=${take}&page=${page}&keyword=${keyword}`
  )
}

function getOffcViewAppoint(take: number, page: number, keyword: string) {
  return http.get(
    `${import.meta.env.VITE_QUERY}/offc-view-appoint?take=${take}&page=${page}&keyword=${keyword}`
  )
}

function getOffcViewAppointByFac(fac_id: string, take: number, page: number, keyword: string) {
  return http.get(
    `${
      import.meta.env.VITE_QUERY
    }/offc-view-appoint-fac?fac_id=${fac_id}&take=${take}&page=${page}&keyword=${keyword}`
  )
}

function getAllAppDocT1(fac_id: string, app_id: number) {
  return http.get(`${import.meta.env.VITE_QUERY}/app-doc-t1?fac_id=${fac_id}&app_id=${app_id}`)
}

function getAllAppDocT2(fac_id: string, app_id: number) {
  return http.get(`${import.meta.env.VITE_QUERY}/app-doc-t2?fac_id=${fac_id}&app_id=${app_id}`)
}

function getAllAppDocT3(fac_id: string, app_id: number) {
  return http.get(`${import.meta.env.VITE_QUERY}/app-doc-t3?fac_id=${fac_id}&app_id=${app_id}`)
}

function getAllApplectLectByAppId(app_id: number) {
  return http.get(`${import.meta.env.VITE_QUERY}/applect-lect-app?app_id=${app_id}`)
}

function getRPLectByTerm(year: string, semester: number) {
  return http.get(
    `${import.meta.env.VITE_QUERY}/rp-applect-lect-term?year=${year}&semester=${semester}`
  )
}

function getNumSpeLectBy(app_id: number) {
  return http.get(`${import.meta.env.VITE_QUERY}/num-spe-lect?app_id=${app_id}`)
}

function getNumCurrBy(app_id: number) {
  return http.get(`${import.meta.env.VITE_QUERY}/num-curr?app_id=${app_id}`)
}

function getNumCourseBy(app_id: number) {
  return http.get(`${import.meta.env.VITE_QUERY}/num-course?app_id=${app_id}`)
}

export default {
  getNumSpeLectBy,
  getNumCurrBy,
  getNumCourseBy,
  getAdminViewAppoint,
  getOffcViewAppoint,
  getOffcViewAppointByFac,
  getAllAppDocT1,
  getAllAppDocT2,
  getAllAppDocT3,
  getAdminViewAppointByFac,
  getAllApplectLectByAppId,
  getRPLectByTerm
}
