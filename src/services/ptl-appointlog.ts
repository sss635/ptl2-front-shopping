import type PtlAppointLogStatus from '@/types/PtlAppointLogStatus'
import http from './axios'

function getAllPtlAppointLogStatuses() {
  return http.get(`${import.meta.env.VITE_PTL_APPOINT_LOGSTS}`)
}

function getOnePtlAppointLogStatus(id: number) {
  return http.get(`${import.meta.env.VITE_PTL_APPOINT_LOGSTS}/${id}`)
}

function savePtlAppointLogStatus(app_id: number, ptlAppointLogStatus: PtlAppointLogStatus) {
  return http.post(`${import.meta.env.VITE_PTL_APPOINT_LOGSTS}/${app_id}`, ptlAppointLogStatus)
}

export default { getAllPtlAppointLogStatuses, getOnePtlAppointLogStatus, savePtlAppointLogStatus }
