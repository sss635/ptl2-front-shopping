import { useVarcompnStore } from './../stores/varcompn'
import axios from 'axios'

const instance = axios.create({
  baseURL: import.meta.env.VITE_BACKEND_URL
})

instance.interceptors.request.use(
  async function (config) {
    localStorage.removeItem('resp_status')
    if (!localStorage.getItem('err_resp_status')?.includes('401')) {
      localStorage.removeItem('err_resp_status')
    }
    const varcompnStore = useVarcompnStore()
    varcompnStore.err_resp_cid = ''

    const token = localStorage.getItem('token')
    if (token) {
      config.headers.Accept = `application/json`
      config.headers.Authorization = `Bearer ${token}`
    }

    return config
  },
  function (error) {
    return Promise.reject(error)
  }
)

instance.interceptors.response.use(
  async function (res) {
    localStorage.setItem('resp_status', res.status + '')
    const varcompnStore = useVarcompnStore()
    varcompnStore.isServiceApi = ''
    return res
  },
  async function (error) {
    localStorage.setItem('err_resp_status', error.response.status)

    const varcompnStore = useVarcompnStore()

    if (
      error.response.data.message &&
      // error.response.status != 401 &&
      error.response.status != 500 &&
      error.response.data.message.includes('ดังกล่าว')
    ) {
      varcompnStore.showMessage(error.response.data.message)
      if (error.response.data.message == 'มีข้อมูลหมายเลขดังกล่าวในระบบอยู่แล้ว') {
        varcompnStore.err_resp_cid = error.response.data.message
      }
    }

    if (
      error.response.data.message &&
      error.response.status != 401 &&
      error.response.status != 500 &&
      varcompnStore.isServiceApi == 's-api'
    ) {
      varcompnStore.showMessage(error.response.data.message)
    }

    if (401 == error.response.status) {
      location.href = '/'
      return Promise.reject(error)
    }
  }
)

export default instance
