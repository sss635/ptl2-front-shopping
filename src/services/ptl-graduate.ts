import type PtlGraduate from '@/types/PtlGraduate'
import http from './axios'

function getAllPtlGraduates() {
  return http.get(`${import.meta.env.VITE_PTL_GRADUATE}`)
}

function getOnePtlGraduate(id: number) {
  return http.get(`${import.meta.env.VITE_PTL_GRADUATE}/${id}`)
}

function savePtlGraduate(lec_id: number, ptlGraduate: PtlGraduate) {
  return http.post(`${import.meta.env.VITE_PTL_GRADUATE}/${lec_id}`, ptlGraduate)
}

function updatePtlGraduate(id: number, ptlGraduate: PtlGraduate) {
  return http.patch(`${import.meta.env.VITE_PTL_GRADUATE}/${id}`, ptlGraduate)
}

function deletePtlGraduate(id: number, ptlGraduate: PtlGraduate) {
  //send data for input DELETE_USER
  return http.delete(`${import.meta.env.VITE_PTL_GRADUATE}/${id}`, { data: ptlGraduate })
}

export default {
  getAllPtlGraduates,
  getOnePtlGraduate,
  savePtlGraduate,
  updatePtlGraduate,
  deletePtlGraduate
}
