import type PtlFacultyPersonPermission from '@/types/PtlFacPermission'
import http from './axios'

function getAllPtlFacultyPersonPermissions() {
  return http.get(`${import.meta.env.VITE_PTL_FAC_PERMISSIONS}`)
}

function getAllPtlFacultyPersonPermissionByPerson(psn_id: string) {
  return http.get(`${import.meta.env.VITE_PTL_FAC_PERMISSIONS}/${psn_id}`)
}

function getOnePtlFacultyPersonPermission(fac_id: string, psn_id: string) {
  return http.get(`${import.meta.env.VITE_PTL_FAC_PERMISSIONS}/${fac_id}/${psn_id}`)
}

function savePtlFacultyPersonPermission(ptlFacultyPersonPermission: PtlFacultyPersonPermission) {
  return http.post(`${import.meta.env.VITE_PTL_FAC_PERMISSIONS}`, ptlFacultyPersonPermission)
}

function updatePtlFacultyPersonPermission(
  fac_id: string,
  psn_id: string,
  ptlFacultyPersonPermission: PtlFacultyPersonPermission
) {
  return http.patch(
    `${import.meta.env.VITE_PTL_FAC_PERMISSIONS}/${fac_id}/${psn_id}`,
    ptlFacultyPersonPermission
  )
}

function deletePtlFacultyPersonPermission(
  fac_id: string,
  psn_id: string,
  ptlFacultyPersonPermission: PtlFacultyPersonPermission
) {
  //send data for input DELETE_USER
  return http.delete(`${import.meta.env.VITE_PTL_FAC_PERMISSIONS}/${fac_id}/${psn_id}`, {
    data: ptlFacultyPersonPermission
  })
}

export default {
  getAllPtlFacultyPersonPermissions,
  getOnePtlFacultyPersonPermission,
  savePtlFacultyPersonPermission,
  updatePtlFacultyPersonPermission,
  deletePtlFacultyPersonPermission,
  getAllPtlFacultyPersonPermissionByPerson
}
