import http from './axios'

function getAllNationalities() {
  return http.get(`${import.meta.env.VITE_SERVICE_API_V1_GET_NATIONALITY}`)
}

function getAllProvinces() {
  return http.get(`${import.meta.env.VITE_SERVICE_API_V1_GET_PROVINCE}`)
}

function getAllAmphursFromProvince(prov_id: string) {
  const dataReqDto = {
    prov_id: prov_id
  }
  return http.post(`${import.meta.env.VITE_SERVICE_API_V1_AMPHUR_FROM_PROVINCE}`, dataReqDto)
}

function getAllTambonsFromProvinceAmphur(prov_id: string, amp_id: string) {
  const dataReqDto = {
    prov_id: prov_id,
    amp_id: amp_id
  }
  return http.post(`${import.meta.env.VITE_SERVICE_API_V1_TAMBON_FROM_PROV_AUM}`, dataReqDto)
}

function getAllCountries() {
  return http.get(`${import.meta.env.VITE_SERVICE_API_V1_GET_COUNTRY}`)
}

function getOneCountry(cty_id: string) {
  const dataReqDto = {
    cty_id: cty_id
  }
  return http.post(`${import.meta.env.VITE_SERVICE_API_V1_GET_COUNTRY}`, dataReqDto)
}

function getAllFaculty() {
  return http.get(`${import.meta.env.VITE_SERVICE_API_V1_GET_FACULTY}`)
}

function getCurriculumFromFaculty(fac_id: string) {
  const dataReqDto = {
    fac_id: fac_id
  }
  return http.post(`${import.meta.env.VITE_SERVICE_API_V1_CURR_FROM_FAC}`, dataReqDto)
}

function getCourseFromCurriculum(curr_id: string) {
  const dataReqDto = {
    curr_id: curr_id
  }
  return http.post(`${import.meta.env.VITE_SERVICE_API_V1_COU_FROM_CURR}`, dataReqDto)
}

function getCourseTeacherFromFaculty(fac_id: string) {
  const dataReqDto = {
    fac_id: fac_id
  }
  return http.post(`${import.meta.env.VITE_SERVICE_API_V1_COU_TCH_FROM_FAC}`, dataReqDto)
}

function getAllGlFaculty() {
  return http.get(`${import.meta.env.VITE_SERVICE_API_V1_GET_GLFACULTY}`)
}

function getGLUserInSystem() {
  return http.post(`${import.meta.env.VITE_SERVICE_API_V1_GET_GLUSERINSYSTEM}`)
}

function getGLGroupInSystem() {
  return http.post(`${import.meta.env.VITE_SERVICE_API_V1_GET_GLGRPINSYSTEM}`)
}

function getCUserGroup(citizenid: string) {
  const dataReqDto = {
    citizenid: citizenid
  }
  return http.post(`${import.meta.env.VITE_SERVICE_API_V1_GET_CUSER_GROUP}`, dataReqDto)
}

function searchPerson(fname: string, lname: string) {
  const dataReqDto = {
    fname: fname,
    lname: lname
  }
  return http.post(`${import.meta.env.VITE_SERVICE_API_V2_GET_SPERSON}`, dataReqDto)
}

function createShortURL(url: string) {
  const dataReqDto = {
    url: url
  }
  return http.post(`${import.meta.env.VITE_SERVICE_API_V2_CREATE_SHORT_URL}`, dataReqDto)
}

function addUserGroup(groupid: string, usid: string) {
  const dataReqDto = {
    groupid: groupid,
    usid: usid
  }
  return http.post(`${import.meta.env.VITE_SERVICE_API_V2_GET_AUSERGROUP}`, dataReqDto)
}

function delUserGroup(groupid: string, usid: string) {
  const dataReqDto = {
    groupid: groupid,
    usid: usid
  }
  return http.delete(`${import.meta.env.VITE_SERVICE_API_V2_GET_DUSERGROUP}`, { data: dataReqDto })
}

function createAccSpeLect(
  facId: string,
  prefixNameTh: string,
  firstNameTh: string,
  middleNameTh: string,
  lastNameTh: string,
  firstNameEng: string,
  middleNameEng: string,
  lastNameEng: string,
  email: string,
  citizenId: string,
  startDate: string,
  endDate: string
) {
  const dataReqDto = {
    facId: facId,
    prefixNameTh: prefixNameTh,
    firstNameTh: firstNameTh,
    middleNameTh: middleNameTh,
    lastNameTh: lastNameTh,
    firstNameEng: firstNameEng,
    middleNameEng: middleNameEng,
    lastNameEng: lastNameEng,
    email: email,
    citizenId: citizenId,
    startDate: startDate,
    endDate: endDate
  }
  return http.post(`${import.meta.env.VITE_SERVICE_API_V1_CREATE_ACCOUNT_SPELECT}`, dataReqDto)
}

export default {
  getAllNationalities,
  getAllProvinces,
  getAllAmphursFromProvince,
  getAllTambonsFromProvinceAmphur,
  getAllCountries,
  getOneCountry,
  getAllFaculty,
  getCurriculumFromFaculty,
  getCourseFromCurriculum,
  getCourseTeacherFromFaculty,
  getAllGlFaculty,
  getGLUserInSystem,
  getGLGroupInSystem,
  getCUserGroup,
  searchPerson,
  createShortURL,
  addUserGroup,
  delUserGroup,
  createAccSpeLect
}
