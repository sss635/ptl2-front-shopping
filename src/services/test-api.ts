import http from './axios'

function sendMail(
  send_system: string,
  send_to: string,
  send_cc: string,
  send_subject: string,
  send_message: string
) {
  const dataReqDto = {
    send_system: send_system,
    send_to: send_to,
    send_cc: send_cc,
    send_subject: send_subject,
    send_message: send_message
  }
  return http.post(`${import.meta.env.VITE_SERVICE_API_V1_MAIL_CENTER_INSERT}`, dataReqDto)
}

function deleteFile(filePath: string) {
  const dataReqDto = {
    filePath: filePath
  }
  return http.delete(`${import.meta.env.VITE_SERVICE_API_V2_DELETE_FILE}`, {
    data: dataReqDto
  })
}

function putFile(fileName: string, originalExtension: string, files: File) {
  const formData = new FormData()

  formData.append('fileName', fileName)

  formData.append('originalExtension', originalExtension)
  formData.append('contentFile', files)

  return http.post(`${import.meta.env.VITE_SERVICE_API_V2_PUT_FILE}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function getPublicFile(filePath: {}, originalFileName: {}) {
  const dataReqDto = {
    filePath: filePath,

    originalFileName: originalFileName
  }

  return http.post(`${import.meta.env.VITE_SERVICE_API_V2_GET_PUBLIC_FILE}`, dataReqDto)
}

export default {
  sendMail,
  deleteFile,
  putFile,
  getPublicFile
}
