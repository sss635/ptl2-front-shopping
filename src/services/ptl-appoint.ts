import type PtlAppoint from '@/types/PtlAppoint'
import http from './axios'

function getAllPtlAppoints() {
  return http.get(`${import.meta.env.VITE_PTL_APPOINT}`)
}

function getAllPtlAppointsWithoutDraft() {
  return http.get(`${import.meta.env.VITE_PTL_APPOINT}/admin-viewer`)
}

function getOnePtlAppoint(id: number) {
  return http.get(`${import.meta.env.VITE_PTL_APPOINT}/${id}`)
}

function getOnePtlAppointThaiNum(id: number) {
  return http.get(`${import.meta.env.VITE_PTL_APPOINT}/thainum/${id}`)
}

function savePtlAppoint(ptlAppoint: PtlAppoint) {
  return http.post(`${import.meta.env.VITE_PTL_APPOINT}`, ptlAppoint)
}

function updatePtlAppoint(id: number, ptlAppoint: PtlAppoint) {
  return http.patch(`${import.meta.env.VITE_PTL_APPOINT}/${id}`, ptlAppoint)
}

function deletePtlAppoint(id: number, ptlAppoint: PtlAppoint) {
  return http.delete(`${import.meta.env.VITE_PTL_APPOINT}/${id}`, { data: ptlAppoint })
}

export default {
  getAllPtlAppoints,
  getOnePtlAppoint,
  savePtlAppoint,
  updatePtlAppoint,
  deletePtlAppoint,
  getAllPtlAppointsWithoutDraft,
  getOnePtlAppointThaiNum
}
