import type PtlAppointDocument from '@/types/PtlAppointDocument'
import http from './axios'

function getAllPtlAppointDocuments() {
  return http.get(`${import.meta.env.VITE_PTL_APPOINT_DOCT}`)
}

function getOnePtlAppointDocument(id: number) {
  return http.get(`${import.meta.env.VITE_PTL_APPOINT_DOCT}/${id}`)
}

function savePtlAppointDocument(app_id: number, ptlAppointDocument: PtlAppointDocument) {
  return http.post(`${import.meta.env.VITE_PTL_APPOINT_DOCT}/${app_id}`, ptlAppointDocument)
}

function updatePtlAppointDocument(id: number, ptlAppointDocument: PtlAppointDocument) {
  return http.patch(`${import.meta.env.VITE_PTL_APPOINT_DOCT}/${id}`, ptlAppointDocument)
}

function deletePtlAppointDocument(id: number, ptlAppointDocument: PtlAppointDocument) {
  //send data for input DELETE_USER
  return http.delete(`${import.meta.env.VITE_PTL_APPOINT_DOCT}/${id}`, { data: ptlAppointDocument })
}

export default {
  getAllPtlAppointDocuments,
  getOnePtlAppointDocument,
  savePtlAppointDocument,
  updatePtlAppointDocument,
  deletePtlAppointDocument
}
