import { ref } from 'vue'
import { defineStore } from 'pinia'
import { useTestPtlStore } from './tests-api'
import type PtlAppoint from '@/types/PtlAppoint'
import { usePtlAppointStore } from './ptl-appoints'
import { usePtlLogEmailStore } from './ptl-log-emails'

export const useVarcompnStore = defineStore('varcompn', () => {
  const tabAppoint = 1
  const tabSpeLect = 2
  const tabYearsms = 3
  const tabPermission = 4
  const tabSetEmail = 5
  const tabReport = 6

  const apiStore = useTestPtlStore()
  const ptlAppointStore = usePtlAppointStore()
  const ptlLogEmailStore = usePtlLogEmailStore()
  const isLoading = ref(false)
  const isShowMessage = ref(false)
  const message = ref('')
  const timeout = ref(2000)
  const pathsIns = ref('/spe-instructor-manage')
  const pathapps = ref('/propose-to')
  const pathsRp = ref('/report')
  const isOpenSelectedRole = ref(false)
  const err_resp_cid = ref('')

  const facAppView = ref('')
  const options = ref([10, 25, 50, 100])

  const defaultStart = 1
  const defaultEnd = 3

  const numre = ref(1)
  const maxre = 2
  const issucc = ref(
    (localStorage.getItem('resp_status')?.includes('201') ||
      localStorage.getItem('resp_status')?.includes('200')) &&
      !localStorage.getItem('err_resp_status')
  )
  const isServiceApi = ref('')

  const showMessage = (msg: string, tout: number = 10000) => {
    message.value = msg
    // if (!localStorage.getItem('err_resp_status')?.includes('401')) {
    isShowMessage.value = true
    timeout.value = tout
    // }
  }

  const closeMessage = () => {
    message.value = ''
    isShowMessage.value = false
  }

  function open(selectRole: string, status: string) {
    if (selectRole == status) {
      console.log('ผู้ดูแลระบบ')
      window.open('./manual/Manual1.pdf', '_blank')
    } else if (selectRole != status) {
      console.log('เจ้าหน้าที่คณะ')
      window.open('./manual/Manual2.pdf', '_blank')
      // window.open('/manual', '_blank');
    }
  }

  const sendStsEmail = async (ptlappoint: PtlAppoint) => {
    const email = ref('')
    const name = ptlappoint?.CREATE_USER || ''
    const urls = 'https://ptl-dev.buu.ac.th'
    const colors = ref('')
    const reason = ref('')
    const txtReasons = ref('')

    email.value = ptlappoint?.APP_OFFC_EMAIL || ''
    if (email.value == '') {
      showMessage('ไม่พบที่อยู่อีเมล')
    }

    if (
      ptlappoint.APP_STATUS == ptlAppointStore.stsReEdit ||
      ptlappoint.APP_STATUS == ptlAppointStore.stsCancel
    ) {
      // console.log('y')
      colors.value = 'red'
      reason.value =
        '<div id="textf">เหตุผล : ' +
        ptlappoint.LOGSTS_LISTS![ptlappoint.LOGSTS_LISTS!.length - 1].APPLS_NOTE +
        '</div>'
      txtReasons.value =
        ' เหตุผล : ' +
        ptlappoint.LOGSTS_LISTS![ptlappoint.LOGSTS_LISTS!.length - 1].APPLS_NOTE +
        ' '
    } else if (
      ptlappoint.APP_STATUS == ptlAppointStore.stsACApproves ||
      ptlappoint.APP_STATUS == ptlAppointStore.stsSuccess
    ) {
      colors.value = 'rgb(0, 150, 140)'
      reason.value = ''
      txtReasons.value = ' '
    } else if (ptlappoint.APP_STATUS == ptlAppointStore.stsUCAckn) {
      colors.value = 'rgb(47, 144, 241)'
      reason.value = ''
      txtReasons.value = ' '
    } else if (
      ptlappoint.APP_STATUS == ptlAppointStore.stsReqAppoint ||
      ptlappoint.APP_STATUS == ptlAppointStore.stsWaitRector
    ) {
      colors.value = 'orange'
      reason.value = ''
      txtReasons.value = ' '
    }

    const temp =
      '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">' +
      '<html>' +
      '<head>' +
      '<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">' +
      // '<style>' +
      // '.textS {' +
      // 'color: ' + colors.value + ';' +
      // '}</style>' +
      '</head>' +
      '<body>' +
      '<div id="pref">เรียน คุณ' +
      name +
      '</div><br>' +
      '<div id="textf">ใบขอเสนอแต่งตั้งเลขที่ : ' +
      ptlappoint.APP_NUMBER +
      '</div> <div id="textS">สถานะการดำเนินงาน : ' +
      '<span style="color: ' +
      colors.value +
      ';">' +
      ptlAppointStore.statusAppoints[ptlappoint.APP_STATUS] +
      '</span></div>' +
      reason.value +
      '<br>ท่านสามารถเข้าไปตรวจสอบและติดตามข้อมูลใบคำขอได้ที่ : <a href="' +
      urls +
      '">' +
      urls +
      '</a>' +
      '</body>' +
      '</html>'
    if (email.value != '') {
      await apiStore.sendMail(
        'ระบบข้อมูลอาจารย์พิเศษ (Part-Time Lecturer)',
        email.value,
        '',
        'แจ้งสถานะดำเนินงานใบคำขอเลขที่ ' + ptlappoint.APP_NUMBER,
        temp
      )
      ptlLogEmailStore.editedPtlLogEmail = {
        EMAIL_RECEIVER: email.value,
        EMAIL_MESSAGE:
          'เรียน คุณ ' +
          name +
          ' ใบขอเสนอแต่งตั้งเลขที่ ' +
          ptlappoint.APP_NUMBER +
          ' สถานะการดำเนินงาน : ' +
          ptlAppointStore.statusAppoints[ptlappoint.APP_STATUS] +
          txtReasons.value +
          'ท่านสามารถเข้าไปตรวจสอบและติดตามข้อมูลใบคำขอได้ที่ : ' +
          urls
      }
      await ptlLogEmailStore.savePtlLogEmail()
    }
  }

  const sendAccEmail = async (
    emailR: string,
    username: string,
    password: string,
    email: string,
    displayName: string,
    passwordExpire: string,
    status: string
  ) => {
    const urlEmail = 'http://email.buu.ac.th'
    const urlCPwd = 'https://myid.buu.ac.th/changepwd'
    const urlResetPwd = 'https://myid.buu.ac.th/recovery'
    const showPwd = ref('')
    const txtPwd = ref('')

    if (status == 'create') {
      showPwd.value =
        '<div id="textf">รหัสผ่าน : ' + password + ' (หมดอายุ ' + passwordExpire + ')</div>'
      txtPwd.value = ' รหัสผ่าน : ' + password + ' (หมดอายุ ' + passwordExpire + ') '
    } else if (status == 'update') {
      showPwd.value = ''
      txtPwd.value = ''
    }

    const temp =
      '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">' +
      '<html>' +
      '<head>' +
      '<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1">' +
      '</head>' +
      '<body>' +
      '<div id="pref">สำนักคอมพิวเตอร์ ขอส่งข้อมูลบัญชีผู้ใช้ มีรายละเอียด ดังนี้ </div><br>' +
      '<div id="textf">บัญชีข้อมูลของผู้ใช้ : ' +
      displayName +
      '</div> ' +
      '<div id="textf">อีเมล : ' +
      email +
      '</div> ' +
      '<div id="textf">ชื่อบัญชี : ' +
      username +
      '</div> ' +
      showPwd.value +
      '<br>รายละเอียดการใช้งานอีเมลสำหรับนิสิตและบุคลากรสามารถดูได้ที่เว็บไซต์ <a href="' +
      urlEmail +
      '">' +
      urlEmail +
      '</a>' +
      '<br><br>1. เปลี่ยนรหัสผ่าน<br> <a href="' +
      urlCPwd +
      '">' +
      urlCPwd +
      '</a>' +
      '<br>2. กรณีลืมรหัสผ่านหรือรหัสผ่านหมดอายุ<br> <a href="' +
      urlResetPwd +
      '">' +
      urlResetPwd +
      '</a>' +
      '<br><br>* หมายเหตุ' +
      '<div id="textf">รหัสผู้ใช้และรหัสผ่านถือเป็นทรัพย์สินส่วนบุคคลที่มีความสำคัญ หากพบว่ารหัสผู้ใช้ของท่านถูกนำไปใช้และเกิดการกระทำผิดตามพระราชบัญญัติว่าด้วยการกระทำความผิดเกี่ยวกับคอมพิวเตอร์ (ฉบับที่ ๒) พ.ศ. ๒๕๖๐ ทางมหาวิทยาลัยจะดำเนินคดีตามกฎหมายต่อไป</div> ' +
      '</body>' +
      '</html>'

    await apiStore.sendMail(
      'ระบบข้อมูลอาจารย์พิเศษ (Part-Time Lecturer)',
      emailR,
      '',
      'แจ้งข้อมูลบัญชีผู้ใช้ ' + displayName,
      temp
    )
    ptlLogEmailStore.editedPtlLogEmail = {
      EMAIL_RECEIVER: emailR,
      EMAIL_MESSAGE:
        'สำนักคอมพิวเตอร์ ขอส่งข้อมูลบัญชีผู้ใช้ มีรายละเอียด ดังนี้ ' +
        'บัญชีข้อมูลของผู้ใช้ : ' +
        displayName +
        ' อีเมล : ' +
        email +
        ' ชื่อบัญชี : ' +
        username +
        txtPwd.value +
        'รายละเอียดการใช้งานอีเมลสำหรับนิสิตและบุคลากรสามารถดูได้ที่เว็บไซต์ ' +
        urlEmail +
        ' 1. เปลี่ยนรหัสผ่าน ' +
        urlCPwd +
        ' 2. กรณีลืมรหัสผ่านหรือรหัสผ่านหมดอายุ ' +
        urlResetPwd +
        ' * หมายเหตุ รหัสผู้ใช้และรหัสผ่านถือเป็นทรัพย์สินส่วนบุคคลที่มีความสำคัญ หากพบว่ารหัสผู้ใช้ของท่านถูกนำไปใช้และเกิดการกระทำผิดตามพระราชบัญญัติว่าด้วยการกระทำความผิดเกี่ยวกับคอมพิวเตอร์ (ฉบับที่ ๒) พ.ศ. ๒๕๖๐ ทางมหาวิทยาลัยจะดำเนินคดีตามกฎหมายต่อไป'
    }
    await ptlLogEmailStore.savePtlLogEmail()
  }

  function delay(time: number) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(true)
      }, time)
    })
  }

  async function redo(func: any) {
    if (localStorage.getItem('err_resp_status')?.includes('401')) {
      localStorage.removeItem('err_resp_status')
      await func
    }
  }

  async function getListPage(
    key: string,
    lastPage: number,
    total: number,
    take: number,
    page: number,
    startP: number,
    endP: number,
    listNum: number[]
  ) {
    lastPage = Math.ceil(total / take)
    let startPNew = 0
    let endPNew = 0
    if (key == 'un') {
      const res = setEnd('un', startP, endP, lastPage)
      page = res.startP
      startPNew = res.startP
      endPNew = res.endP
    } else if (key == '0') {
      const res = setStart('un', startP, endP, lastPage, listNum)
      page = res.startP
      startPNew = res.startP
      endPNew = res.endP
    } else if (key == 'h') {
      const res = setStart('h', startP, endP, lastPage, listNum)
      page = defaultStart
      startPNew = res.startP
      endPNew = res.endP
    } else if (key == 't') {
      const res = setEnd('t', startP, endP, lastPage)
      page = res.startP
      startPNew = res.startP
      endPNew = res.endP
    }
    const resList = reList(startPNew, endPNew)
    return { lastPage, total, take, page, startPNew, endPNew, resList }
  }

  function reList(startP: number, endP: number) {
    const listNum = <number[]>[]
    for (let i = startP; i <= endP; i++) {
      listNum.push(i)
    }
    return listNum
  }

  function setEnd(key: string, startP: number, endP: number, lastPage: number) {
    startP += defaultEnd
    endP += defaultEnd
    if ((endP > lastPage && lastPage >= defaultEnd) || key == 't') {
      if (lastPage % defaultEnd != 0) {
        startP = lastPage - (lastPage % defaultEnd) + 1
      } else {
        startP = lastPage - defaultEnd + 1
      }
      endP = lastPage
    }
    if (lastPage < defaultEnd) {
      startP = defaultStart
      endP = lastPage
    }
    return { startP, endP }
  }

  function setStart(
    key: string,
    startP: number,
    endP: number,
    lastPage: number,
    listNum: number[]
  ) {
    startP -= defaultEnd
    if (listNum.length != defaultEnd) {
      endP -= lastPage % defaultEnd
    } else {
      endP -= defaultEnd
    }

    if (startP < 1 || endP < defaultEnd || key == 'h') {
      startP = defaultStart
      endP = defaultEnd
    }
    if (lastPage < defaultEnd) {
      endP = lastPage
    }
    return { startP, endP }
  }

  function proveSEdateforRP(
    yrs_sday: string,
    yrs_smonth: string,
    yrs_syear: string,
    yrs_eday: string,
    yrs_emonth: string,
    yrs_eyear: string,
    start_timeh: string,
    end_timeh: string
  ) {
    yrs_syear = parseInt(yrs_syear) + 543 + ''
    yrs_eyear = parseInt(yrs_eyear) + 543 + ''
    if (parseInt(start_timeh) + 7 < 24 && parseInt(end_timeh) + 7 >= 24) {
      yrs_eday = parseInt(yrs_eday) + 1 + ''
    } else if (parseInt(start_timeh) + 7 >= 24 && parseInt(end_timeh) + 7 >= 24) {
      yrs_sday = parseInt(yrs_sday) + 1 + ''
      yrs_eday = parseInt(yrs_eday) + 1 + ''
    } else if (parseInt(start_timeh) + 7 >= 24 && parseInt(end_timeh) + 7 < 24) {
      yrs_sday = parseInt(yrs_sday) + 1 + ''
    }
    const start_date = yrs_sday + '/' + yrs_smonth + '/' + yrs_syear
    const end_date = yrs_eday + '/' + yrs_emonth + '/' + yrs_eyear
    return { start_date, end_date }
  }

  function proveDateforTable(
    yrs_day: string,
    yrs_month: string,
    yrs_year: string,
    timeh: string,
    timem: string
  ) {
    yrs_year = parseInt(yrs_year) + 543 + ''
    if (parseInt(timeh) + 7 < 24) {
      timeh = parseInt(timeh) + 7 + ''
    } else if (parseInt(timeh) + 7 >= 24) {
      yrs_day = parseInt(yrs_day) + 1 + ''
      timeh = '0' + (parseInt(timeh) - 24 + 7)
    }
    const date = yrs_day + '/' + yrs_month + '/' + yrs_year
    const datewtime = yrs_day + '/' + yrs_month + '/' + yrs_year + ' ' + timeh + timem
    return { date, datewtime }
  }

  return {
    isLoading,
    isShowMessage,
    message,
    timeout,
    showMessage,
    closeMessage,
    pathsIns,
    pathapps,
    pathsRp,
    isOpenSelectedRole,
    err_resp_cid,
    facAppView,
    options,
    numre,
    maxre,
    redo,
    issucc,
    isServiceApi,
    sendStsEmail,
    sendAccEmail,
    defaultStart,
    defaultEnd,
    getListPage,
    reList,
    open,
    proveSEdateforRP,
    proveDateforTable,
    delay,
    tabAppoint,
    tabSpeLect,
    tabYearsms,
    tabPermission,
    tabSetEmail,
    tabReport
  }
})
