import { ref } from 'vue'
import { defineStore } from 'pinia'
import type PtlAppointCourseTeacher from '@/types/PtlAppointCourseTeacher'
import { useVarcompnStore } from './varcompn'
import ptlAppointCourseTeacherService from '@/services/ptl-appointcourse'

export const usePtlAppointCourseTeacherStore = defineStore('ptlAppointCourseTeacher', () => {
  const varcompnStore = useVarcompnStore()
  const ptlAppointCourseTeachers = ref<PtlAppointCourseTeacher[]>([])
  const editedPtlAppointCourseTeacher = ref<PtlAppointCourseTeacher>({
    PRF_NAMETH: '',
    PSN_FNAMETH: '',
    PSN_LNAMETH: ''
  })

  function clearPtlAppointCourseTeacher() {
    editedPtlAppointCourseTeacher.value = {
      PRF_NAMETH: '',
      PSN_FNAMETH: '',
      PSN_LNAMETH: ''
    }
  }

  async function getAllPtlAppointCourseTeachers() {
    try {
      const res = await ptlAppointCourseTeacherService.getAllPtlAppointCourseTeachers()
      ptlAppointCourseTeachers.value = res.data
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(getAllPtlAppointCourseTeachers())
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลอาจารย์ประจำรายวิชาได้')
      }
    }
  }

  async function getOnePtlAppointCourseTeacher(appl_id: number, psn_id: string) {
    try {
      const res = await ptlAppointCourseTeacherService.getOnePtlAppointCourseTeacher(
        appl_id,
        psn_id
      )

      editedPtlAppointCourseTeacher.value = res.data
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(getOnePtlAppointCourseTeacher(appl_id, psn_id))
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลอาจารย์ประจำรายวิชารายนี้ได้')
      }
    }
  }

  async function savePtlAppointCourseTeacher(appl_id: number) {
    try {
      if (editedPtlAppointCourseTeacher.value.APPL_ID) {
        editedPtlAppointCourseTeacher.value.UPDATE_USER = localStorage.getItem('nameonly')!
        editedPtlAppointCourseTeacher.value.UPDATE_PROG = 'PtlAppointCourseTeacher'
        await ptlAppointCourseTeacherService.updatePtlAppointCourseTeacher(
          editedPtlAppointCourseTeacher.value.APPL_ID!,
          editedPtlAppointCourseTeacher.value.PSN_ID!,
          editedPtlAppointCourseTeacher.value
        )
      } else {
        editedPtlAppointCourseTeacher.value.CREATE_USER = localStorage.getItem('nameonly')!
        await ptlAppointCourseTeacherService.savePtlAppointCourseTeacher(
          appl_id,
          editedPtlAppointCourseTeacher.value
        )
      }

      await getAllPtlAppointCourseTeachers()
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(savePtlAppointCourseTeacher(appl_id))
      } else {
        varcompnStore.showMessage('ไม่สามารถบันทึกข้อมูลอาจารย์ประจำรายวิชาได้')
      }
    }
  }

  async function editPtlAppointCourseTeacher(ptlAppointCourseTeacher: PtlAppointCourseTeacher) {
    editedPtlAppointCourseTeacher.value = ptlAppointCourseTeacher
    editedPtlAppointCourseTeacher.value.APPL_ID = ptlAppointCourseTeacher.APPLECT!.APPL_ID
  }

  async function deletePtlAppointCourseTeacher(ptlAppointCourseTeacher: PtlAppointCourseTeacher) {
    try {
      ptlAppointCourseTeacher.DELETE_USER = localStorage.getItem('nameonly')!
      await ptlAppointCourseTeacherService.deletePtlAppointCourseTeacher(
        ptlAppointCourseTeacher.APPL_ID!,
        ptlAppointCourseTeacher.PSN_ID!,
        ptlAppointCourseTeacher
      )

      await getAllPtlAppointCourseTeachers()
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(deletePtlAppointCourseTeacher(ptlAppointCourseTeacher))
      } else {
        varcompnStore.showMessage('ไม่สามารถลบข้อมูลอาจารย์ประจำรายวิชาได้')
      }
    }
  }

  return {
    ptlAppointCourseTeachers,
    getAllPtlAppointCourseTeachers,
    editedPtlAppointCourseTeacher,
    savePtlAppointCourseTeacher,
    editPtlAppointCourseTeacher,
    deletePtlAppointCourseTeacher,
    clearPtlAppointCourseTeacher,
    getOnePtlAppointCourseTeacher
  }
})
