import { ref } from 'vue'
import { defineStore } from 'pinia'
import router from '@/router'
import loginService from '@/services/login'
import { useVarcompnStore } from './varcompn'
import { useTestPtlStore } from './tests-api'
import ptlFacPermissionService from '@/services/ptl-fac-permission'

export const useLoginStore = defineStore('login', () => {
  const loginReqToken = ref({
    user_login: '',
    user_password: '',
    client_id: '',
    secret: ''
  })
  const ststoken = ref('')

  const loginBuu = ref({
    username: '',
    password: ''
  })
  const resBuu = ref('')

  const usernames = ref('')
  const selectgpid = ref('')
  const selectgpname = ref('')
  const id = ref('')
  const name = ref('')
  const citizenidd = ref('')
  const fac_id = ref('')
  const fac_name = ref('')
  const per_fac_id = ref('')
  const per_fac_name = ref('')

  const resPersonData = ref()
  const resUserGroup = ref()

  const varcompnStore = useVarcompnStore()
  const testPtlStore = useTestPtlStore()

  const loginForBuu = async (username: string, password: string): Promise<void> => {
    varcompnStore.isLoading = true
    try {
      varcompnStore.isServiceApi = 's-api'
      localStorage.removeItem('token')
      const res = await loginService.loginBuu(username, password)

      localStorage.setItem('username', username)
      usernames.value = localStorage.getItem('username')!
      localStorage.setItem('resbuu', JSON.stringify(res.data))
      resBuu.value = JSON.parse(localStorage.getItem('resbuu')!)

      localStorage.setItem('token', res.data.token)
      localStorage.setItem('per-email', res.data.result.email)
      localStorage.setItem('psn_id', res.data.person.result.psn_id)
      id.value = localStorage.getItem('psn_id')!
      localStorage.setItem(
        'name',
        res.data.person.result.prf_nameth +
          res.data.person.result.psn_fnameth +
          ' ' +
          res.data.person.result.psn_lnameth
      )
      name.value = localStorage.getItem('name')!
      localStorage.setItem(
        'nameonly',
        res.data.person.result.psn_fnameth + ' ' + res.data.person.result.psn_lnameth
      )
      localStorage.setItem('citizenid', res.data.person.result.psn_idcard)
      citizenidd.value = localStorage.getItem('citizenid')!
      localStorage.setItem('fac_id', res.data.person.result.fac_id)
      fac_id.value = localStorage.getItem('fac_id')!
      per_fac_id.value = localStorage.getItem('fac_id')!
      localStorage.setItem('fac_nameth', res.data.person.result.fac_nameth)
      fac_name.value = localStorage.getItem('fac_nameth')!
      localStorage.setItem('per_fac_nameth', res.data.person.result.fac_nameth)
      per_fac_name.value = localStorage.getItem('per_fac_nameth')!
      const gpids = ref('')
      for (let i = 0; i < res.data.person.uGroup.result.length; i++) {
        gpids.value += res.data.person.uGroup.result[i].gpid
        if (i < res.data.person.uGroup.result.length - 1) {
          gpids.value += ', '
        }
      }
      localStorage.setItem('selectgpid', gpids.value)

      selectgpid.value = localStorage.getItem('selectgpid')!
      const gpnamets = ref('')
      for (let i = 0; i < res.data.person.uGroup.result.length; i++) {
        gpnamets.value += res.data.person.uGroup.result[i].gpnamet
        if (i < res.data.person.uGroup.result.length - 1) {
          gpnamets.value += ', '
        }
      }
      localStorage.setItem('selectgpname', gpnamets.value)
      selectgpname.value = localStorage.getItem('selectgpname')!

      localStorage.setItem('selectRole', res.data.person.uGroup.result[0].gpid)
      localStorage.setItem('selectRoleName', res.data.person.uGroup.result[0].gpnamet)
      await refreshToken()

      if (res.data.person.uGroup.result[0].gpid == '402') {
        await refreshFacList()
      }

      localStorage.setItem('numgpid', res.data.person.uGroup.result.length)
      const numgpid = parseInt(localStorage.getItem('numgpid')!)
      if (numgpid > 0) {
        window.location.href = '/propose-to'
      } else {
        varcompnStore.showMessage(
          'คุณยังไม่มีสิทธิ์เข้าถึงระบบนี้ โปรดติดต่อผู้ดูแลระบบหากคิดว่านี่เป็นข้อผิดพลาด'
        )
      }
    } catch (err) {
      // console.log(err);
    }
    varcompnStore.isLoading = false
  }

  async function refreshFacList() {
    const facLists = ref<string[]>([])
    const resPer = await ptlFacPermissionService.getAllPtlFacultyPersonPermissionByPerson(id.value)
    if (resPer.data.length > 0) {
      for (let j = 0; j < resPer.data.length; j++) {
        facLists.value.push(resPer.data[j].FAC_ID)
      }
    }
    if (facLists.value.length == 0) {
      facLists.value.push(per_fac_id.value)
    }
    localStorage.setItem('facLists', facLists.value.toString())
  }

  const logoutBuu = (): void => {
    localStorage.removeItem('token')
    localStorage.removeItem('token-api')
    localStorage.removeItem('ststoken')
    localStorage.removeItem('resbuu')

    localStorage.removeItem('ressend')
    localStorage.removeItem('resfiledel')
    localStorage.removeItem('resadd')
    localStorage.removeItem('resgetfile')

    localStorage.removeItem('username')
    localStorage.removeItem('selectgpid')
    localStorage.removeItem('selectgpname')
    localStorage.removeItem('psn_id')
    localStorage.removeItem('name')
    localStorage.removeItem('citizenid')
    localStorage.removeItem('fac_id')
    localStorage.removeItem('fac_nameth')
    localStorage.removeItem('per_fac_nameth')

    localStorage.removeItem('facLists')
    localStorage.removeItem('err_resp_status')
    localStorage.removeItem('resp_status')
    localStorage.removeItem('selectRole')
    localStorage.removeItem('selectRoleName')

    localStorage.removeItem('numgpid')
    localStorage.removeItem('per-email')
    localStorage.removeItem('nameonly')

    router.push('/')

    loadData()
  }

  const loadData = () => {
    localStorage.getItem('token')
    localStorage.getItem('token-api')
    ststoken.value = JSON.parse(localStorage.getItem('ststoken')!) || ''

    usernames.value = localStorage.getItem('username')! || ''
    selectgpid.value = localStorage.getItem('selectgpid')! || ''
    selectgpname.value = localStorage.getItem('selectgpname')! || ''
    id.value = localStorage.getItem('psn_id')! || ''
    name.value = localStorage.getItem('name')! || ''
    citizenidd.value = localStorage.getItem('citizenid')! || ''
    fac_id.value = localStorage.getItem('fac_id')! || ''
    fac_name.value = localStorage.getItem('fac_nameth')! || ''
    per_fac_id.value = localStorage.getItem('fac_id')! || ''
    per_fac_name.value = localStorage.getItem('per_fac_nameth')! || ''

    resBuu.value = JSON.parse(localStorage.getItem('resbuu')!) || ''
    testPtlStore.resSend = JSON.parse(localStorage.getItem('ressend')!) || ''
    testPtlStore.resDelFile = JSON.parse(localStorage.getItem('resfiledel')!) || ''
    testPtlStore.resAddFile = JSON.parse(localStorage.getItem('resadd')!) || ''
    testPtlStore.resGetFile = JSON.parse(localStorage.getItem('resgetfile')!) || ''

    localStorage.getItem('facLists')
    localStorage.getItem('err_resp_status')
    localStorage.getItem('resp_status')
    localStorage.getItem('selectRole')
    localStorage.getItem('selectRoleName')
  }

  const refreshToken = async (): Promise<void> => {
    try {
      const res = await loginService.refreshToken()

      localStorage.setItem('token', res.data.token)
      localStorage.setItem('ststoken', JSON.stringify(res.data))
      ststoken.value = JSON.parse(localStorage.getItem('ststoken')!)
    } catch (err) {
      // console.log(err);
    }
  }

  return {
    loginReqToken,
    ststoken,
    loadData,
    loginBuu,
    loginForBuu,
    usernames,
    resBuu,
    logoutBuu,
    id,
    name,
    citizenidd,
    fac_id,
    fac_name,
    per_fac_id,
    per_fac_name,
    resPersonData,
    selectgpid,
    selectgpname,
    resUserGroup,
    refreshToken,
    refreshFacList
  }
})
