import queryService from '@/services/query'
import { ref } from 'vue'
import { defineStore } from 'pinia'
import type PtlAppointDocument from '@/types/PtlAppointDocument'
import { useVarcompnStore } from './varcompn'
import ptlAppointDocumentService from '@/services/ptl-appointdoc'
import { useTestPtlStore } from './tests-api'

export const usePtlAppointDocumentStore = defineStore('ptlAppointDocument', () => {
  const varcompnStore = useVarcompnStore()
  const testPtls = useTestPtlStore()

  const ptlAppointDocuments = ref<PtlAppointDocument[]>([])
  const ptlAppointDocumentsT1 = ref<PtlAppointDocument[]>([])
  const ptlAppointDocumentsT2 = ref<PtlAppointDocument[]>([])
  const ptlAppointDocumentsT3 = ref<PtlAppointDocument[]>([])
  const editedPtlAppointDocument = ref<PtlAppointDocument & { files: File[] }>({
    APPD_NAME: '',
    APPD_FILE_NAME: '',
    APPD_TYPE: 1,
    files: []
  })
  const typeAppointDocuments = [
    { valuem: 1, type: 'เอกสารมติคณะกรรมการประจำส่วนงาน' },
    { valuem: 2, type: 'เอกสารแผนในการบริหารจัดการ' }
  ]

  function clearPtlAppointDocument() {
    editedPtlAppointDocument.value = {
      APPD_NAME: '',
      APPD_FILE_NAME: '',
      APPD_TYPE: 1,
      files: []
    }
  }

  async function getAllPtlAppointDocuments() {
    varcompnStore.isLoading = true
    try {
      const res = await ptlAppointDocumentService.getAllPtlAppointDocuments()
      ptlAppointDocuments.value = res.data
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await (getAllPtlAppointDocuments())
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลเอกสารทั้งหมดได้')
      }
    }
    varcompnStore.isLoading = false
  }

  async function getAllPtlAppointDocumentsT1(fac_id: string, app_id: number) {
    varcompnStore.isLoading = true
    try {
      const res = await queryService.getAllAppDocT1(fac_id, app_id)
      const tempres = res.data
      ptlAppointDocumentsT1.value = []
      for (let i = 0; i < tempres.length; i++) {
        const datewtime = varcompnStore.proveDateforTable(
          tempres[i].UPDATE_DATE?.substring(8, 10),
          tempres[i].UPDATE_DATE?.substring(5, 7),
          tempres[i].UPDATE_DATE?.substring(0, 4),
          tempres[i].UPDATE_DATE?.substring(11, 13),
          tempres[i].UPDATE_DATE?.substring(13, 16)
        ).datewtime
        tempres[i].UPDATE_DATE = datewtime
        ptlAppointDocumentsT1.value.push(tempres[i])
      }
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await (getAllPtlAppointDocumentsT1(fac_id, app_id))
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลเอกสารมติคณะกรรมการประจำส่วนงานนี้ได้')
      }
    }
    varcompnStore.isLoading = false
  }

  async function getAllPtlAppointDocumentsT2(fac_id: string, app_id: number) {
    varcompnStore.isLoading = true
    try {
      const res = await queryService.getAllAppDocT2(fac_id, app_id)
      const tempres = res.data
      ptlAppointDocumentsT2.value = []
      for (let i = 0; i < tempres.length; i++) {
        const datewtime = varcompnStore.proveDateforTable(
          tempres[i].UPDATE_DATE?.substring(8, 10),
          tempres[i].UPDATE_DATE?.substring(5, 7),
          tempres[i].UPDATE_DATE?.substring(0, 4),
          tempres[i].UPDATE_DATE?.substring(11, 13),
          tempres[i].UPDATE_DATE?.substring(13, 16)
        ).datewtime
        tempres[i].UPDATE_DATE = datewtime
        ptlAppointDocumentsT2.value.push(tempres[i])
      }
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await (getAllPtlAppointDocumentsT2(fac_id, app_id))
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลเอกสารแผนในการบรืหารจัดการได้')
      }
    }
    varcompnStore.isLoading = false
  }

  async function getAllPtlAppointDocumentsT3(fac_id: string, app_id: number) {
    varcompnStore.isLoading = true
    try {
      const res = await queryService.getAllAppDocT3(fac_id, app_id)
      const tempres = res.data
      ptlAppointDocumentsT3.value = []
      for (let i = 0; i < tempres.length; i++) {
        const datewtime = varcompnStore.proveDateforTable(
          tempres[i].UPDATE_DATE?.substring(8, 10),
          tempres[i].UPDATE_DATE?.substring(5, 7),
          tempres[i].UPDATE_DATE?.substring(0, 4),
          tempres[i].UPDATE_DATE?.substring(11, 13),
          tempres[i].UPDATE_DATE?.substring(13, 16)
        ).datewtime
        tempres[i].UPDATE_DATE = datewtime
        ptlAppointDocumentsT3.value.push(tempres[i])
      }
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await (getAllPtlAppointDocumentsT3(fac_id, app_id))
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลเอกสารคำสั่งแต่งตั้งได้')
      }
    }
    varcompnStore.isLoading = false
  }

  async function getOnePtlAppointDocument(id: number) {
    varcompnStore.isLoading = true
    try {
      const res = await ptlAppointDocumentService.getOnePtlAppointDocument(id)

      editedPtlAppointDocument.value = res.data
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await (getOnePtlAppointDocument(id))
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลเอกสารนี้ได้')
      }
    }
    varcompnStore.isLoading = false
  }

  async function savePtlAppointDocument(APP_ID: number, files: File, fac_id: string) {
    try {
      if (files && files.name) {
        const ext = files.name.split('.')
        const originalExtension = ext[1]
        await testPtls.putFile('', originalExtension, files)
        const resName = testPtls.getFilenameFromPut

        if (
          editedPtlAppointDocument.value.APPD_FILE_NAME != '' &&
          editedPtlAppointDocument.value.APPD_FILE_NAME != '-'
        ) {
          await testPtls.deleteFile(editedPtlAppointDocument.value.APPD_FILE_NAME!)
        }
        editedPtlAppointDocument.value.APPD_FILE_NAME = resName
      }
      if (editedPtlAppointDocument.value.APPD_ID) {
        editedPtlAppointDocument.value.UPDATE_USER = localStorage.getItem('nameonly')!
        editedPtlAppointDocument.value.UPDATE_PROG = 'PtlAppointDocument'
        await ptlAppointDocumentService.updatePtlAppointDocument(
          editedPtlAppointDocument.value.APPD_ID,
          editedPtlAppointDocument.value
        )
      } else {
        editedPtlAppointDocument.value.CREATE_USER = localStorage.getItem('nameonly')!
        await ptlAppointDocumentService.savePtlAppointDocument(
          APP_ID,
          editedPtlAppointDocument.value
        )
      }
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await (savePtlAppointDocument(APP_ID, files, fac_id))
      } else {
        if (editedPtlAppointDocument.value.APPD_TYPE == 1) {
          await getAllPtlAppointDocumentsT1(fac_id, APP_ID)
        } else if (editedPtlAppointDocument.value.APPD_TYPE == 2) {
          await getAllPtlAppointDocumentsT2(fac_id, APP_ID)
        } else if (editedPtlAppointDocument.value.APPD_TYPE == 3) {
          await getAllPtlAppointDocumentsT3(fac_id, APP_ID)
        }
      }
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await (savePtlAppointDocument(APP_ID, files, fac_id))
      } else {
        varcompnStore.showMessage('ไม่สามารถบันทึกข้อมูลเอกสารได้')
      }
    }
  }

  async function updateByDeleteFile(fac_id: string, ptlAppointDocument: PtlAppointDocument) {
    try {
      await testPtls.deleteFile(ptlAppointDocument.APPD_FILE_NAME!)
      ptlAppointDocument.APPD_FILE_NAME = '-'
      ptlAppointDocument.UPDATE_USER = localStorage.getItem('nameonly')!
      ptlAppointDocument.UPDATE_PROG = 'PtlAppointDocument'
      await ptlAppointDocumentService.updatePtlAppointDocument(
        ptlAppointDocument.APPD_ID!,
        ptlAppointDocument
      )
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await (updateByDeleteFile(fac_id, ptlAppointDocument))
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถอัปเดตข้อมูลเอกสารด้วยการลบไฟล์ได้')
      }
    }
  }

  async function editPtlAppointDocument(ptlAppointDocument: PtlAppointDocument) {
    varcompnStore.isLoading = true
    editedPtlAppointDocument.value = JSON.parse(JSON.stringify(ptlAppointDocument))
    editedPtlAppointDocument.value.APP_ID = ptlAppointDocument.APPOINT!.APP_ID
    varcompnStore.isLoading = false
  }

  async function deletePtlAppointDocument(ptlAppointDocument: PtlAppointDocument, fac_id: string) {
    try {
      ptlAppointDocument.DELETE_USER = localStorage.getItem('nameonly')!
      if (ptlAppointDocument.APPD_FILE_NAME != '-') {
        await testPtls.deleteFile(ptlAppointDocument.APPD_FILE_NAME!)
      }

      await ptlAppointDocumentService.deletePtlAppointDocument(
        ptlAppointDocument.APPD_ID!,
        ptlAppointDocument
      )

      if (ptlAppointDocument.APPD_TYPE == 1) {
        await getAllPtlAppointDocumentsT1(fac_id, ptlAppointDocument.APP_ID!)
      } else if (ptlAppointDocument.APPD_TYPE == 2) {
        await getAllPtlAppointDocumentsT2(fac_id, ptlAppointDocument.APP_ID!)
      } else if (editedPtlAppointDocument.value.APPD_TYPE == 3) {
        await getAllPtlAppointDocumentsT3(fac_id, ptlAppointDocument.APP_ID!)
      }
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await (deletePtlAppointDocument(ptlAppointDocument, fac_id))
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถลบข้อมูลเอกสารนี้ได้')
      }
    }
  }

  return {
    ptlAppointDocuments,
    getAllPtlAppointDocuments,
    editedPtlAppointDocument,
    savePtlAppointDocument,
    editPtlAppointDocument,
    deletePtlAppointDocument,
    clearPtlAppointDocument,
    getOnePtlAppointDocument,
    typeAppointDocuments,
    getAllPtlAppointDocumentsT1,
    getAllPtlAppointDocumentsT2,
    ptlAppointDocumentsT1,
    ptlAppointDocumentsT2,
    updateByDeleteFile,
    getAllPtlAppointDocumentsT3,
    ptlAppointDocumentsT3
  }
})
