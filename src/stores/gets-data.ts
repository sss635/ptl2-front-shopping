import { ref } from 'vue'
import { defineStore } from 'pinia'
import getData from '@/services/get-data'
import { useVarcompnStore } from './varcompn'
import { useLoginStore } from './logins'
import { usePtlFacultyPersonPermissionStore } from './ptl-fac-permissions'
import type PtlFacultyPersonPermission from '@/types/PtlFacPermission'

export const useGetDataStore = defineStore('get-data', () => {
  const logins = useLoginStore()
  const allNationality = ref()
  const allProvince = ref()
  const allAmphursFromProvince = ref()
  const allTambonsFromProvinceAmphur = ref()
  const allCountry = ref()
  const get_CTY_NAMETH = ref('')
  const get_CTY = ref('')
  const allFaculty = ref()
  const allCurrFromFac = ref()
  const allCouFromCurr = ref()
  const allCouTchFromFac = ref()
  const perFacItem = ref()
  const appFacId = ref()
  const appFacName = ref()
  const allFacultyByRole = ref<{}[]>([])
  const selectCurrId = ref()
  const selectCurr = ref()
  const selectCouId = ref()
  const selectCourse = ref()
  const allGlFaculty = ref()
  const gLUserInSystem = ref()
  const gLGroupInSystem = ref()
  const cUserGroup = ref()
  const srchPerson = ref()
  const resCreateAccSpeLect = ref()

  const isAppOffc = ref('')
  const shortUrl = ref('')
  const ptlPerfStore = usePtlFacultyPersonPermissionStore()
  const glus = ref<boolean[]>([])
  const gluFacs = ref<PtlFacultyPersonPermission[][]>([])

  const varcompnStore = useVarcompnStore()

  const getAllNationalities = async (): Promise<void> => {
    varcompnStore.isServiceApi = 's-api'
    try {
      const res = await getData.getAllNationalities()
      allNationality.value = res.data.result
    } catch (err) {
      // console.log(err);
    }
  }

  const getAllProvinces = async (): Promise<void> => {
    varcompnStore.isServiceApi = 's-api'
    try {
      const res = await getData.getAllProvinces()
      allProvince.value = res.data.result
    } catch (err) {
      // console.log(err);
    }
  }

  const getAllAmphursFromProvince = async (prov_id_a: string): Promise<void> => {
    varcompnStore.isServiceApi = 's-api'
    try {
      const res = await getData.getAllAmphursFromProvince(prov_id_a)
      allAmphursFromProvince.value = res.data.result
      if (allAmphursFromProvince.value == undefined) {
        varcompnStore.showMessage('ไม่พบข้อมูลอำเภอของจังหวัดที่เลือก')
      }
    } catch (err) {
      // console.log(err);
    }
  }

  const getAllTambonsFromProvinceAmphur = async (
    prov_id: string,
    amp_id: string
  ): Promise<void> => {
    varcompnStore.isServiceApi = 's-api'
    try {
      const res = await getData.getAllTambonsFromProvinceAmphur(prov_id, amp_id)
      allTambonsFromProvinceAmphur.value = res.data.result
      if (allTambonsFromProvinceAmphur.value == undefined) {
        varcompnStore.showMessage('ไม่พบข้อมูลตำบลของอำเภอที่เลือก')
      }
    } catch (err) {
      // console.log(err);
      allTambonsFromProvinceAmphur.value = ''
    }
  }

  const getAllCountries = async (): Promise<void> => {
    varcompnStore.isServiceApi = 's-api'
    try {
      const res = await getData.getAllCountries()
      allCountry.value = res.data.result
    } catch (err) {
      // console.log(err);
    }
  }

  const getOneCountry = async (cty_id: string): Promise<void> => {
    varcompnStore.isServiceApi = 's-api'
    try {
      const res = await getData.getOneCountry(cty_id)
      get_CTY_NAMETH.value = res.data.result.CTY_NAMETH
      get_CTY.value = res.data.result
    } catch (err) {
      // console.log(err);
    }
  }

  const getAllFaculty = async (): Promise<void> => {
    varcompnStore.isServiceApi = 's-api'
    try {
      const res = await getData.getAllFaculty()
      allFaculty.value = await res.data.result
      const uGroup = localStorage.getItem('selectRole')
      if (isAppOffc.value == 'app-offc') {
        if (uGroup == '402') {
          await logins.refreshFacList()
        }
        const facRoleLists = localStorage.getItem('facLists')?.split(',') || ['']
        allFacultyByRole.value = []
        await genFacRoleFor(facRoleLists)
      }
    } catch (err) {
      // console.log(err);
    }
  }

  function genFacRoleFor(facRoleLists: string[]) {
    for (const item of allFaculty.value) {
      if (facRoleLists?.includes(item.fac_id)) {
        allFacultyByRole.value.push(item)
      }
      perFacItem.value = allFacultyByRole.value[0]
    }
  }

  const getAllFacultyForAD = async (): Promise<void> => {
    varcompnStore.isServiceApi = 's-api'
    try {
      const res = await getData.getAllFaculty()
      allFaculty.value = res.data.result
      for (const item of allFaculty.value) {
        if (item.fac_id == appFacId.value) {
          appFacName.value = item.fac_nameth
        }
      }
    } catch (err) {
      // console.log(err);
    }
  }

  const getCurriculumFromFaculty = async (fac_id: string): Promise<void> => {
    varcompnStore.isServiceApi = 's-api'
    try {
      const res = await getData.getCurriculumFromFaculty(fac_id)
      // // console.log(res.data);
      allCurrFromFac.value = res.data.result
      for (const item of allCurrFromFac.value) {
        if (item.CURR_ID == selectCurrId.value) {
          selectCurr.value = item
        }
      }
    } catch (err) {
      // console.log(err);
    }
  }

  const getCourseFromCurriculum = async (curr_id: string): Promise<void> => {
    varcompnStore.isServiceApi = 's-api'
    try {
      const res = await getData.getCourseFromCurriculum(curr_id)
      allCouFromCurr.value = res.data.result
      for (const item of allCouFromCurr.value) {
        if (item.COU_CODE.includes(selectCouId.value)) {
          selectCourse.value = item
        }
      }
    } catch (err) {
      // console.log(err);
    }
  }

  const getCourseTeacherFromFaculty = async (fac_id: string): Promise<void> => {
    varcompnStore.isServiceApi = 's-api'
    try {
      const res = await getData.getCourseTeacherFromFaculty(fac_id)
      allCouTchFromFac.value = res.data.result
    } catch (err) {
      // console.log(err);
    }
  }

  async function getAllGlFaculty() {
    varcompnStore.isServiceApi = 's-api'
    try {
      const res = await getData.getAllGlFaculty()
      allGlFaculty.value = res.data.result
    } catch (err) {
      // console.log(err);
    }
  }

  async function getGLUserInSystem() {
    varcompnStore.isServiceApi = 's-api'
    try {
      const res = await getData.getGLUserInSystem()
      gLUserInSystem.value = res.data.result
      glus.value = []
      gluFacs.value = []
      await checkPer()
    } catch (err) {
      // console.log(err);
    }
  }

  async function checkPer() {
    for (const r of gLUserInSystem.value) {
      await ptlPerfStore.getAllPtlFacultyPersonPermissionByPerson(r.psn_id)
      // if(ptlPerfStore.ptlFacultyPersonPermissionsByPer.length==0) {
      glus.value.push(ptlPerfStore.ptlFacultyPersonPermissionsByPer.length == 0)
      // if (ptlPerfStore.ptlFacultyPersonPermissionsByPer.length > 0) {
      gluFacs.value.push(ptlPerfStore.ptlFacultyPersonPermissionsByPer)
      // }
      // }
    }
  }

  async function getGLGroupInSystem() {
    varcompnStore.isServiceApi = 's-api'
    try {
      const res = await getData.getGLGroupInSystem()
      gLGroupInSystem.value = res.data.result
    } catch (err) {
      // console.log(err);
    }
  }

  async function getCUserGroup(citizenid: string) {
    varcompnStore.isServiceApi = 's-api'
    try {
      const res = await getData.getCUserGroup(citizenid)
      if (res.data.result) {
        cUserGroup.value = res.data.result
      }
    } catch (err) {
      // console.log(err);
    }
  }

  async function searchPerson(fname: string, lname: string) {
    varcompnStore.isServiceApi = 's-api'
    try {
      const res = await getData.searchPerson(fname, lname)
      srchPerson.value = res.data.result
      gluFacs.value = []
      await checkPerSrh()
    } catch (err) {
      // console.log(err);
    }
  }

  async function checkPerSrh() {
    for (const srh of srchPerson.value) {
      await ptlPerfStore.getAllPtlFacultyPersonPermissionByPerson(srh.psn_id)
      gluFacs.value.push(ptlPerfStore.ptlFacultyPersonPermissionsByPer)
    }
  }

  async function createShortURL(url: string) {
    varcompnStore.isLoading = true
    varcompnStore.isServiceApi = 's-api'
    try {
      const res = await getData.createShortURL(url)
      shortUrl.value = res.data.shortUrl
    } catch (err) {
      // console.log(err);
    }
    varcompnStore.isLoading = false
  }

  async function addUserGroup(groupid: string, usid: string) {
    varcompnStore.isServiceApi = 's-api'
    try {
      const res = await getData.addUserGroup(groupid, usid)
    } catch (err) {
      // console.log(err);
    }
  }

  async function delUserGroup(groupid: string, usid: string) {
    varcompnStore.isServiceApi = 's-api'
    try {
      const res = await getData.delUserGroup(groupid, usid)
    } catch (err) {
      // console.log(err);
    }
  }

  async function createAccSpeLect(
    facId: string,
    prefixNameTh: string,
    firstNameTh: string,
    middleNameTh: string,
    lastNameTh: string,
    firstNameEng: string,
    middleNameEng: string,
    lastNameEng: string,
    email: string,
    citizenId: string,
    startDate: string,
    endDate: string
  ) {
    varcompnStore.isServiceApi = 's-api'
    try {
      const res = await getData.createAccSpeLect(
        facId,
        prefixNameTh,
        firstNameTh,
        middleNameTh,
        lastNameTh,
        firstNameEng,
        middleNameEng,
        lastNameEng,
        email,
        citizenId,
        startDate,
        endDate
      )
      resCreateAccSpeLect.value = res.data
      // resCreateAccSpeLect.value = res.data.result
    } catch (err) {
      // console.log(err);
    }
  }

  return {
    getAllNationalities,
    allNationality,
    getAllProvinces,
    allProvince,
    getAllAmphursFromProvince,
    allAmphursFromProvince,
    getAllTambonsFromProvinceAmphur,
    allTambonsFromProvinceAmphur,
    getAllCountries,
    getOneCountry,
    allCountry,
    get_CTY_NAMETH,
    get_CTY,
    getAllFaculty,
    getCurriculumFromFaculty,
    getCourseFromCurriculum,
    getCourseTeacherFromFaculty,
    allFaculty,
    allCurrFromFac,
    allCouFromCurr,
    allCouTchFromFac,
    perFacItem,
    allFacultyByRole,
    appFacId,
    appFacName,
    getAllFacultyForAD,
    selectCurr,
    selectCurrId,
    selectCouId,
    selectCourse,
    getAllGlFaculty,
    getGLUserInSystem,
    getGLGroupInSystem,
    getCUserGroup,
    searchPerson,
    allGlFaculty,
    gLUserInSystem,
    gLGroupInSystem,
    cUserGroup,
    srchPerson,
    isAppOffc,
    shortUrl,
    createShortURL,
    addUserGroup,
    delUserGroup,
    glus,
    gluFacs,
    createAccSpeLect,
    resCreateAccSpeLect
  }
})
