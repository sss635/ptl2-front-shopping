import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
import type PtlAppoint from '@/types/PtlAppoint'
import { useVarcompnStore } from './varcompn'
import ptlAppointService from '@/services/ptl-appoint'
import router from '@/router'
import queryService from '@/services/query'

export const usePtlAppointStore = defineStore('ptlAppoint', () => {
  const stsDraft = 1
  const stsReqAppoint = 2
  const stsReEdit = 3
  const stsACApproves = 4
  const stsWaitRector = 8
  const stsUCAckn = 5
  const stsSuccess = 6
  const stsCancel = 7

  const varcompnStore = useVarcompnStore()
  const ptlAppoints = ref<PtlAppoint[]>([])
  const ptlAppointsByFac = ref<PtlAppoint[]>([])
  const ptlAppointsWithOutDraft = ref<PtlAppoint[]>([])
  const editedPtlAppoint = ref<PtlAppoint>({
    FAC_ID: '',
    FAC_NAMETH: '',
    APP_STATUS: stsDraft,
    APP_OFFC_EMAIL: ''
  })
  const ptlAppoint = ref<PtlAppoint>()
  const statusAppoints: { [key: number]: string } = {
    1: 'ร่าง',
    2: 'ขอเสนอแต่งตั้ง',
    3: 'ส่งกลับแก้ไข',
    4: 'สภาวิชาการเห็นชอบ',
    8: 'อยู่ระหว่างเสนออธิการบดี',
    5: 'สภามหาวิยาลัยรับทราบ',
    6: 'ดำเนินการเรียบร้อยแล้ว',
    7: 'ยกเลิก'
  }
  const statusAppointsAd = [
    { valuem: stsReqAppoint, status: 'ขอเสนอแต่งตั้ง' },
    { valuem: stsReEdit, status: 'ส่งกลับแก้ไข' },
    { valuem: stsACApproves, status: 'สภาวิชาการเห็นชอบ' },
    { valuem: stsWaitRector, status: 'อยู่ระหว่างเสนออธิการบดี' },
    { valuem: stsUCAckn, status: 'สภามหาวิยาลัยรับทราบ' },
    { valuem: stsSuccess, status: 'ดำเนินการเรียบร้อยแล้ว' },
    { valuem: stsCancel, status: 'ยกเลิก' }
  ]

  const numSpeLectLists = ref<number[]>([])
  const numCurrLists = ref<number[]>([])
  const numCourseLists = ref<number[]>([])
  const lastPageA = ref(1)
  const pageA = ref(1)
  const takeA = ref(10)
  const totalA = ref(0)
  const keywordA = ref('')
  const facidA = ref('')
  const lastPageO = ref(1)
  const pageO = ref(1)
  const takeO = ref(10)
  const totalO = ref(0)
  const keywordO = ref('')
  const facidO = ref('')
  const allApplectLectByAppId = ref<{}[]>([])
  const appointLects = ref<{}[]>([])
  const listPerc = ref<number[]>([])
  const listNumapl = ref<number[]>([])
  const listQ = ref<number[]>([])
  const listLecCou = ref<number[]>([])

  watch(keywordA, async (newKeyword, oldKeyword) => {
    if (facidA.value != '') {
      await getAllPtlAppointsWithoutDraftByFac(
        facidA.value,
        takeA.value,
        pageA.value,
        keywordA.value
      )
    } else {
      await getAllPtlAppointsWithoutDraft(takeA.value, pageA.value, keywordA.value)
    }
  })

  watch(lastPageA, async (newLastPage, oldLastPage) => {
    if (newLastPage < pageA.value) {
      pageA.value = 1
      if (facidA.value != '') {
        await getAllPtlAppointsWithoutDraftByFac(
          facidA.value,
          takeA.value,
          pageA.value,
          keywordA.value
        )
      } else {
        await getAllPtlAppointsWithoutDraft(takeA.value, pageA.value, keywordA.value)
      }
    }
  })

  watch(keywordO, async (newKeyword, oldKeyword) => {
    await getPtlAppointsByFac(facidO.value, takeO.value, pageO.value, keywordO.value)
  })

  watch(lastPageO, async (newLastPage, oldLastPage) => {
    if (newLastPage < pageO.value) {
      pageO.value = 1
      await getPtlAppointsByFac(facidO.value, takeO.value, pageO.value, keywordO.value)
    }
  })

  function clearPtlAppoint() {
    editedPtlAppoint.value = {
      FAC_ID: '',
      FAC_NAMETH: '',
      APP_STATUS: stsDraft,
      YRS_ID: -1,
      APP_OFFC_EMAIL: ''
    }
  }

  async function getAllPtlAppoints(take: number, page: number, keyword: string) {
    try {
      const res = await queryService.getOffcViewAppoint(take, page, keyword)
      ptlAppoints.value = res.data.data
      lastPageO.value = res.data.lastPage
      totalO.value = res.data.count
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await (getAllPtlAppoints(take, page, keyword))
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลขอเสนอแต่งตั้งทั้งหมดได้')
      }
    }
  }

  async function getPtlAppointsByFac(fac_id: string, take: number, page: number, keyword: string) {
    try {
      facidO.value = fac_id

      const res = await queryService.getOffcViewAppointByFac(fac_id, take, page, keyword)
      const tempres = res.data.data
      // console.log(tempres)
      ptlAppointsByFac.value = []
      for (let i = 0; i < tempres.length; i++) {
        const datewtime = varcompnStore.proveDateforTable(
          tempres[i].UPDATE_DATE?.substring(8, 10),
          tempres[i].UPDATE_DATE?.substring(5, 7),
          tempres[i].UPDATE_DATE?.substring(0, 4),
          tempres[i].UPDATE_DATE?.substring(11, 13),
          tempres[i].UPDATE_DATE?.substring(13, 16)
        ).datewtime
        tempres[i].UPDATE_DATE = datewtime
        ptlAppointsByFac.value.push(tempres[i])
        // await getOnePtlAppoint(tempres[i].APP_ID)
      }
      lastPageO.value = res.data.lastPage
      totalO.value = res.data.count
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await (getPtlAppointsByFac(fac_id, take, page, keyword))
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลขอเสนอแต่งตั้งของคณะนี้ได้')
      }
    }
  }

  async function getAllPtlAppointsWithoutDraft(takeA: number, pageA: number, keywordA: string) {
    try {
      const res = await queryService.getAdminViewAppoint(takeA, pageA, keywordA)
      const tempres = res.data.data
      ptlAppointsWithOutDraft.value = []
      for (let i = 0; i < tempres.length; i++) {
        const datewtime = varcompnStore.proveDateforTable(
          tempres[i].UPDATE_DATE?.substring(8, 10),
          tempres[i].UPDATE_DATE?.substring(5, 7),
          tempres[i].UPDATE_DATE?.substring(0, 4),
          tempres[i].UPDATE_DATE?.substring(11, 13),
          tempres[i].UPDATE_DATE?.substring(13, 16)
        ).datewtime
        tempres[i].UPDATE_DATE = datewtime
        ptlAppointsWithOutDraft.value.push(tempres[i])
      }
      lastPageA.value = res.data.lastPage
      totalA.value = res.data.count
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await (getAllPtlAppointsWithoutDraft(takeA, pageA, keywordA))
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลขอเสนอแต่งตั้งทั้งหมดได้')
      }
    }
  }

  async function getAllPtlAppointsWithoutDraftByFac(
    fac_id: string,
    takeA: number,
    pageA: number,
    keywordA: string
  ) {
    try {
      facidA.value = fac_id
      const res = await queryService.getAdminViewAppointByFac(fac_id, takeA, pageA, keywordA)
      const tempres = res.data.data
      ptlAppointsWithOutDraft.value = []
      for (let i = 0; i < tempres.length; i++) {
        const datewtime = varcompnStore.proveDateforTable(
          tempres[i].UPDATE_DATE?.substring(8, 10),
          tempres[i].UPDATE_DATE?.substring(5, 7),
          tempres[i].UPDATE_DATE?.substring(0, 4),
          tempres[i].UPDATE_DATE?.substring(11, 13),
          tempres[i].UPDATE_DATE?.substring(13, 16)
        ).datewtime
        tempres[i].UPDATE_DATE = datewtime
        ptlAppointsWithOutDraft.value.push(tempres[i])
      }
      lastPageA.value = res.data.lastPage
      totalA.value = res.data.count
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await (getAllPtlAppointsWithoutDraftByFac(fac_id, takeA, pageA, keywordA))
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลขอเสนอแต่งตั้งของคณะนี้ได้')
      }
    }
  }

  async function getOnePtlAppoint(id: number) {
    varcompnStore.isLoading = true
    try {
      const res = await ptlAppointService.getOnePtlAppoint(id)
      editedPtlAppoint.value = res.data
      ptlAppoint.value = res.data
      const tempres = res.data?.APPLECT_LISTS
      appointLects.value = []
      let q = 0
      listPerc.value = []
      for (let i = 0; i < tempres.length; i++) {
        listPerc.value.push(0)
        if (
          i == 0 ||
          (i > 0 && tempres[i].CURR_NAMETH != tempres[i - 1].CURR_NAMETH) ||
          tempres[i].CURR_YEAR != tempres[i - 1].CURR_YEAR
        ) {
          q = i
          listPerc.value[q] += parseFloat(tempres[i].APPL_PERCENT)
        }
        if (
          i > 0 &&
          tempres[i].CURR_ID == tempres[i - 1].CURR_ID &&
          tempres[i].COU_CODE == tempres[i - 1].COU_CODE &&
          tempres[i].APPL_GROUP == tempres[i - 1].APPL_GROUP &&
          tempres[i].LECT.LEC_ID != tempres[i - 1].LECT.LEC_ID
        ) {
          listPerc.value[q] += parseFloat(tempres[i].APPL_PERCENT)
          listPerc.value[i] = listPerc.value[q]
          // console.log(tempres[i].CURR_ID,tempres[i].COU_CODE,tempres[i].APPL_GROUP,tempres[i].LECT.LEC_ID)
        }
        // listPerc.value[q] += parseFloat(tempres[i].APPL_PERCENT)
        appointLects.value.push(tempres[i])
      }
      // console.log(listPerc.value)
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await (getOnePtlAppoint(id))
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลขอเสนอแต่งตั้งนี้ได้')
      }
    }
    varcompnStore.isLoading = false
  }

  async function getOnePtlAppointThaiNum(id: number) {
    varcompnStore.isLoading = true
    try {
      const res = await ptlAppointService.getOnePtlAppointThaiNum(id)
      editedPtlAppoint.value = res.data
      ptlAppoint.value = res.data
      const tempres = res.data?.APPLECT_LISTS
      appointLects.value = []
      let n = 0
      listNumapl.value = []
      let q = 0
      listQ.value = []
      let nlecc = 0
      listLecCou.value = []
      for (let i = 0; i < tempres.length; i++) {
        listNumapl.value.push(0)
        listLecCou.value.push(0)

        if (
          i == 0 ||
          (i > 0 && tempres[i].CURR_NAMETH != tempres[i - 1].CURR_NAMETH) ||
          tempres[i].CURR_YEAR != tempres[i - 1].CURR_YEAR
        ) {
          n = i
          listNumapl.value[n]++
          q++
          listQ.value.push(q)
          nlecc = i
          listLecCou.value[nlecc]++
        }

        if (
          i > 0 &&
          tempres[i].CURR_ID == tempres[i - 1].CURR_ID &&
          tempres[i].LECT.LEC_ID == tempres[i - 1].LECT.LEC_ID
        ) {
          listNumapl.value[n]++
          listNumapl.value[i] = listNumapl.value[n]
          listQ.value.push(0)
          if (tempres[i].COU_CODE == tempres[i - 1].COU_CODE) {
            listLecCou.value[nlecc]++
            listLecCou.value[i] = listNumapl.value[nlecc]
          } else if (tempres[i].COU_CODE != tempres[i - 1].COU_CODE) {
            listLecCou.value[i]++
          }
        } else if (
          i > 0 &&
          tempres[i].CURR_ID == tempres[i - 1].CURR_ID &&
          tempres[i].LECT.LEC_ID != tempres[i - 1].LECT.LEC_ID
        ) {
          n = i
          listNumapl.value[n]++
          q++
          listQ.value.push(q)
          if (tempres[i].COU_CODE == tempres[i - 1].COU_CODE) {
            nlecc = i
            listLecCou.value[nlecc]++
          } else if (tempres[i].COU_CODE != tempres[i - 1].COU_CODE) {
            listLecCou.value[i]++
          }
        }

        appointLects.value.push(tempres[i])
      }
      // console.log(listNumapl.value)
      // console.log(listLecCou.value)
      // console.log(listQ.value)
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await (getOnePtlAppointThaiNum(id))
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลขอเสนอแต่งตั้งนี้ได้')
      }
    }
    varcompnStore.isLoading = false
  }

  async function getAllApplectLectByAppId(id: number) {
    varcompnStore.isLoading = true
    try {
      const res = await queryService.getAllApplectLectByAppId(id)
      const tempres = res.data
      allApplectLectByAppId.value = []
      for (let i = 0; i < tempres.length; i++) {
        const datewtime = varcompnStore.proveDateforTable(
          tempres[i].LEC_USERNAME_CREATE?.substring(8, 10),
          tempres[i].LEC_USERNAME_CREATE?.substring(5, 7),
          tempres[i].LEC_USERNAME_CREATE?.substring(0, 4),
          tempres[i].LEC_USERNAME_CREATE?.substring(11, 13),
          tempres[i].LEC_USERNAME_CREATE?.substring(13, 16)
        ).datewtime
        tempres[i].LEC_USERNAME_CREATE = datewtime
        allApplectLectByAppId.value.push(tempres[i])
      }
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await (getAllApplectLectByAppId(id))
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลอาจารย์ในคำขอเสนอแต่งตั้งนี้ได้')
      }
    }
    varcompnStore.isLoading = false
  }

  async function savePtlAppoint() {
    varcompnStore.isLoading = true
    try {
      if (editedPtlAppoint.value.APP_ID) {
        editedPtlAppoint.value.UPDATE_USER = localStorage.getItem('nameonly')!
        editedPtlAppoint.value.UPDATE_PROG = 'PtlAppoint'

        await ptlAppointService.updatePtlAppoint(
          editedPtlAppoint.value.APP_ID,
          editedPtlAppoint.value
        )
      } else {
        editedPtlAppoint.value.CREATE_USER = localStorage.getItem('nameonly')!
        editedPtlAppoint.value.APP_OFFC_EMAIL = localStorage.getItem('per-email') + ''
        await ptlAppointService.savePtlAppoint(editedPtlAppoint.value)
      }

      // if (localStorage.getItem('err_resp_status')?.includes('401')) {
      //   await varcompnStore.redo(savePtlAppoint())
      // }

      await getPtlAppointsByFac(
        editedPtlAppoint.value.FAC_ID!,
        takeO.value,
        pageO.value,
        keywordO.value
      )
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await varcompnStore.redo(savePtlAppoint())
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถบันทึกข้อมูลขอเสนอแต่งตั้งได้')
      }
    }
    varcompnStore.isLoading = false
  }

  async function editPtlAppoint(ptlAppoint: PtlAppoint, enc_appid: string, enc_facid: string) {
    varcompnStore.isLoading = true

    router.push({
      path: '/offc-propose-to/add-appoint-propose',
      query: { type: 'edit', app_id: enc_appid, fac_id: enc_facid }
    })

    varcompnStore.pathapps = '/offc-propose-to'
  }

  async function deletePtlAppoint(ptlAppoint: PtlAppoint) {
    varcompnStore.isLoading = true
    try {
      ptlAppoint.DELETE_USER = localStorage.getItem('nameonly')!

      await ptlAppointService.updatePtlAppoint(ptlAppoint.APP_ID!, ptlAppoint)
      await ptlAppointService.deletePtlAppoint(ptlAppoint.APP_ID!, ptlAppoint)

      await getPtlAppointsByFac(ptlAppoint.FAC_ID!, takeO.value, pageO.value, keywordO.value)
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await (deletePtlAppoint(ptlAppoint))
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถลบข้อมูลขอเสนอแต่งตั้งได้')
      }
    }
    varcompnStore.isLoading = false
  }

  return {
    ptlAppoints,
    getAllPtlAppoints,
    editedPtlAppoint,
    savePtlAppoint,
    editPtlAppoint,
    deletePtlAppoint,
    clearPtlAppoint,
    getOnePtlAppoint,
    statusAppoints,
    getAllPtlAppointsWithoutDraft,
    ptlAppointsWithOutDraft,
    ptlAppoint,
    numSpeLectLists,
    numCurrLists,
    numCourseLists,
    getPtlAppointsByFac,
    ptlAppointsByFac,
    getAllPtlAppointsWithoutDraftByFac,
    lastPageA,
    pageA,
    takeA,
    keywordA,
    lastPageO,
    pageO,
    takeO,
    keywordO,
    getOnePtlAppointThaiNum,
    statusAppointsAd,
    getAllApplectLectByAppId,
    allApplectLectByAppId,
    totalA,
    totalO,
    stsDraft,
    stsReqAppoint,
    stsReEdit,
    stsACApproves,
    stsUCAckn,
    stsSuccess,
    stsCancel,
    appointLects,
    listPerc,
    listNumapl,
    listQ,
    listLecCou,
    stsWaitRector
  }
})
