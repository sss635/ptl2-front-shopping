import { ref } from 'vue'
import { defineStore } from 'pinia'
import type PtlAppointLecturer from '@/types/PtlAppointLecturer'
import { useVarcompnStore } from './varcompn'
import ptlAppointLecturerService from '@/services/ptl-appointlect'
import router from '@/router'

export const usePtlAppointLecturerStore = defineStore('ptlAppointLecturer', () => {
  const varcompnStore = useVarcompnStore()
  const ptlAppointLecturers = ref<PtlAppointLecturer[]>([])
  const ptlAppointLecturer = ref<PtlAppointLecturer>()
  const ptlAppointLecturer_startDate = ref('')
  const ptlAppointLecturer_endDate = ref('')
  const editedPtlAppointLecturer = ref<PtlAppointLecturer>({
    CURR_ID: '',
    CURR_YEAR: '',
    CURR_NAMETH: '',
    CURR_NAMEEN: '',
    CURR_STATUS: '',
    CURR_STATUS_NAME: '',
    COU_CODE: '',
    COU_YEAR: '',
    COU_NAMETH: '',
    COU_NAMEEN: '',
    COU_CREDIT1: '',
    COU_CREDIT2: '',
    COU_CREDIT3: '',
    COU_CREDIT4: '',
    APPL_GROUP: '',
    APPL_HOUR: 0,
    APPL_PERCENT: 0,
    APPL_YEAR: '',
    APPL_SEMESTER: 1,
    LEV_ID: 1,
    LEV_NAMETH: '',
    APPL_RATIO: '',
    APPL_REASON: '',
    APPCOURSE_LISTS: []
  })
  const typeAppointSemesters = [
    { valuem: 1, type: 'ต้น' },
    { valuem: 2, type: 'ปลาย' },
    { valuem: 3, type: 'ฤดูร้อน' }
  ]
  const date = new Date()
  const year = date.getFullYear()
  const yearth = year + 543
  const yearths = []
  for (let i = yearth - 5; i <= yearth + 5; i++) {
    yearths.push(i)
  }
  const typeApprove = [
    { valuem: 'Y', type: 'อนุมัติ' },
    { valuem: 'N', type: 'ไม่อนุมัติ' },
    { valuem: null, type: '-' }
  ]

  function getApprove(appl_status: string) {
    for (const t of typeApprove) {
      if (t.valuem == appl_status) {
        return t.type
      }
    }
  }

  function clearPtlAppointLecturer() {
    editedPtlAppointLecturer.value = {
      CURR_ID: '',
      CURR_YEAR: '',
      CURR_NAMETH: '',
      CURR_NAMEEN: '',
      CURR_STATUS: '',
      CURR_STATUS_NAME: '',
      COU_CODE: '',
      COU_YEAR: '',
      COU_NAMETH: '',
      COU_NAMEEN: '',
      COU_CREDIT1: '',
      COU_CREDIT2: '',
      COU_CREDIT3: '',
      COU_CREDIT4: '',
      APPL_GROUP: '',
      APPL_HOUR: 0,
      APPL_PERCENT: 0,
      APPL_YEAR: '',
      APPL_SEMESTER: 1,
      LEV_ID: 1,
      LEV_NAMETH: '',
      APPL_RATIO: '',
      APPL_REASON: '',
      APPCOURSE_LISTS: []
    }
  }

  async function getAllPtlAppointLecturers() {
    try {
      const res = await ptlAppointLecturerService.getAllPtlAppointLecturers()
      ptlAppointLecturers.value = res.data
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(getAllPtlAppointLecturers())
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลอาจารย์พิเศษที่แต่งตั้งได้')
      }
    }
  }

  async function getOnePtlAppointLecturer(id: number) {
    try {
      const res = await ptlAppointLecturerService.getOnePtlAppointLecturer(id)

      ptlAppointLecturer.value = res.data
      ptlAppointLecturer.value!.APPOINT = res.data.APPOINT
      ptlAppointLecturer.value!.LECT = res.data.LECT
      ptlAppointLecturer.value!.YEARSEMESTER = res.data.YEARSEMESTER
      editedPtlAppointLecturer.value = JSON.parse(JSON.stringify(res.data))
      editedPtlAppointLecturer.value.APP_ID = res.data.APPOINT?.APP_ID
      editedPtlAppointLecturer.value.LEC_ID = res.data.LECT?.LEC_ID
      const tempstart = ptlAppointLecturer.value!.YEARSEMESTER!.YRS_START_DATE + ''
      const tempend = ptlAppointLecturer.value!.YEARSEMESTER!.YRS_END_DATE + ''
      const { start_date, end_date } = varcompnStore.proveSEdateforRP(
        tempstart.substring(8, 10),
        tempstart.substring(5, 7),
        tempstart.substring(0, 4),
        tempend.substring(8, 10),
        tempend.substring(5, 7),
        tempend.substring(0, 4),
        '',
        ''
      )
      ptlAppointLecturer_startDate.value = start_date
      ptlAppointLecturer_endDate.value = end_date
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(getOnePtlAppointLecturer(id))
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลอาจารย์พิเศษที่แต่งตั้งนี้ได้')
      }
    }
  }

  async function savePtlAppointLecturer(
    APP_ID: number,
    FAC_ID: number,
    enc_appid: string,
    enc_facid: string
  ) {
    varcompnStore.isLoading = true
    try {
      if (editedPtlAppointLecturer.value.APPL_ID) {
        editedPtlAppointLecturer.value.UPDATE_USER = localStorage.getItem('nameonly')!
        editedPtlAppointLecturer.value.UPDATE_PROG = 'PtlAppointLecturer'
        await ptlAppointLecturerService.updatePtlAppointLecturer(
          editedPtlAppointLecturer.value.APPL_ID,
          editedPtlAppointLecturer.value
        )
      } else {
        editedPtlAppointLecturer.value.CREATE_USER = localStorage.getItem('nameonly')!
        await ptlAppointLecturerService.savePtlAppointLecturer(
          APP_ID,
          editedPtlAppointLecturer.value
        )
      }

      if (
        (localStorage.getItem('resp_status')?.includes('201') ||
          localStorage.getItem('resp_status')?.includes('200')) &&
        !localStorage.getItem('err_resp_status')?.includes('400') &&
        !localStorage.getItem('err_resp_status')?.includes('401') &&
        !localStorage.getItem('err_resp_status')?.includes('500')
      ) {
        await getAllPtlAppointLecturers()

        const ch = await ptlAppointLecturerService.getOnePtlAppointLecturerCheck(
          APP_ID,
          editedPtlAppointLecturer.value
        )
        // console.log(ch)
        if (ch != undefined) {
          router.replace({
            path: '/offc-propose-to/add-appoint-propose',
            query: { type: 'edit', app_id: enc_appid, fac_id: enc_facid }
          })
        }
      }
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(savePtlAppointLecturer(APP_ID, FAC_ID, enc_appid, enc_facid))
      } else {
        varcompnStore.showMessage('ไม่สามารถบันทึกข้อมูลอาจารย์พิเศษที่แต่งตั้งได้')
      }
    }
    varcompnStore.isLoading = false
  }

  async function savePtlAppointLecturerByAd(appointLecturer: PtlAppointLecturer) {
    // varcompnStore.isLoading = true
    try {
      appointLecturer.UPDATE_USER = localStorage.getItem('nameonly')!
      appointLecturer.UPDATE_PROG = 'PtlAppointLecturer'

      await ptlAppointLecturerService.updatePtlAppointLecturer(
        appointLecturer.APPL_ID!,
        appointLecturer
      )
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(savePtlAppointLecturerByAd(appointLecturer))
      } else {
        varcompnStore.showMessage('ไม่สามารถบันทึกการอนุมัติข้อมูลอาจารย์พิเศษที่แต่งตั้งได้')
      }
    }
    // varcompnStore.isLoading = false
  }

  async function editPtlAppointLecturer(
    ptlAppointLecturer: PtlAppointLecturer,
    enc_appid: string,
    enc_facid: string,
    enc_applid: string
  ) {
    router.replace({
      path: '/offc-propose-to/add-appoint-propose/add-appoint-lect',
      query: { type: 'edit', app_id: enc_appid, fac_id: enc_facid, appl_id: enc_applid }
    })
    varcompnStore.pathapps = '/offc-propose-to'
  }

  async function deletePtlAppointLecturer(ptlAppointLecturer: PtlAppointLecturer) {
    try {
      ptlAppointLecturer.DELETE_USER = localStorage.getItem('nameonly')!
      await ptlAppointLecturerService.deletePtlAppointLecturer(
        ptlAppointLecturer.APPL_ID!,
        ptlAppointLecturer
      )

      await getAllPtlAppointLecturers()
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(deletePtlAppointLecturer(ptlAppointLecturer))
      } else {
        varcompnStore.showMessage('ไม่สามารถลบข้อมูลอาจารย์พิเศษที่แต่งตั้งได้')
      }
    }
  }

  return {
    ptlAppointLecturers,
    getAllPtlAppointLecturers,
    editedPtlAppointLecturer,
    savePtlAppointLecturer,
    editPtlAppointLecturer,
    deletePtlAppointLecturer,
    clearPtlAppointLecturer,
    getOnePtlAppointLecturer,
    typeAppointSemesters,
    yearths,
    ptlAppointLecturer,
    typeApprove,
    savePtlAppointLecturerByAd,
    getApprove,
    ptlAppointLecturer_startDate,
    ptlAppointLecturer_endDate
  }
})
