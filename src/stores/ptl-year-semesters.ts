import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
import type PtlYearSemester from '@/types/PtlYearSemester'
import { useVarcompnStore } from './varcompn'
import ptlYearSemesterService from '@/services/ptl-year-semester'

export const usePtlYearSemesterStore = defineStore('PtlYearSemester', () => {
  const ismodal = ref('')
  const varcompnStore = useVarcompnStore()
  const ptlYearSemesters = ref<PtlYearSemester[]>([])
  const ptlYearSemestersManP = ref<PtlYearSemester[]>([])
  const ptlYearSemester = ref<PtlYearSemester[]>([])
  const editedPtlYearSemester = ref<PtlYearSemester>({
    YRS_YEAR: '',
    YRS_SEMESTER: 0
  })
  const typeAppointSemesters = [
    { valuem: 1, type: 'ต้น' },
    { valuem: 2, type: 'ปลาย' },
    { valuem: 3, type: 'ฤดูร้อน' }
  ]
  const date = new Date()
  const year = date.getFullYear()
  const yearth = year + 543
  const yearths = []
  for (let i = yearth - 5; i <= yearth + 5; i++) {
    yearths.push(i + '')
  }
  const lastPage = ref(1)
  const page = ref(1)
  const take = ref(10)
  const keyword = ref('')
  const total = ref(0)

  watch(keyword, async (newKeyword, oldKeyword) => {
    await getAllPtlYearSemestersByManP(take.value, page.value, keyword.value)
  })

  watch(lastPage, async (newLastPage, oldLastPage) => {
    if (newLastPage < page.value) {
      page.value = 1
      await getAllPtlYearSemestersByManP(take.value, page.value, keyword.value)
    }
  })

  function clearPtlYearSemester() {
    editedPtlYearSemester.value = {
      YRS_YEAR: '',
      YRS_SEMESTER: 0
    }
  }

  async function getAllPtlYearSemesters() {
    varcompnStore.isLoading = true
    try {
      const res = await ptlYearSemesterService.getAllPtlYearSemesters()
      ptlYearSemesters.value = res.data
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await (getAllPtlYearSemesters())
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลปี/ภาคการศึกษาได้')
      }
    }
    varcompnStore.isLoading = false
  }

  async function getAllPtlYearSemestersByManP(take: number, page: number, keyword: string) {
    try {
      const res = await ptlYearSemesterService.getAllPtlYearSemestersByManP(take, page, keyword)
      // console.log(res.data)
      const tempres = res.data.data
      ptlYearSemestersManP.value = []
      for (let i = 0; i < tempres.length; i++) {
        const { start_date, end_date } = varcompnStore.proveSEdateforRP(
          tempres[i].YRS_START_DATE.substring(8, 10),
          tempres[i].YRS_START_DATE.substring(5, 7),
          tempres[i].YRS_START_DATE.substring(0, 4),
          tempres[i].YRS_END_DATE.substring(8, 10),
          tempres[i].YRS_END_DATE.substring(5, 7),
          tempres[i].YRS_END_DATE.substring(0, 4),
          '',
          ''
        )
        tempres[i].YRS_START_DATE = start_date
        tempres[i].YRS_END_DATE = end_date
        ptlYearSemestersManP.value.push(tempres[i])
      }
      lastPage.value = res.data.lastPage
      total.value = res.data.count
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await (getAllPtlYearSemestersByManP(take, page, keyword))
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลปี/ภาคการศึกษาได้')
      }
    }
  }

  async function getOnePtlYearSemester(id: number) {
    try {
      const res = await ptlYearSemesterService.getOnePtlYearSemester(id)

      editedPtlYearSemester.value = res.data
      ptlYearSemester.value = res.data
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await (getOnePtlYearSemester(id))
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลปี/ภาคการศึกษานี้ได้')
      }
    }
  }

  async function savePtlYearSemester() {
    varcompnStore.isLoading = true
    try {
      if (editedPtlYearSemester.value.YRS_ID) {
        editedPtlYearSemester.value.UPDATE_USER = localStorage.getItem('nameonly')!
        editedPtlYearSemester.value.UPDATE_PROG = 'PtlYearSemester'
        await ptlYearSemesterService.updatePtlYearSemester(
          editedPtlYearSemester.value.YRS_ID,
          editedPtlYearSemester.value
        )
      } else {
        editedPtlYearSemester.value.CREATE_USER = localStorage.getItem('nameonly')!
        await ptlYearSemesterService.savePtlYearSemester(editedPtlYearSemester.value)
      }
      // if (localStorage.getItem('err_resp_status')?.includes('401')) {
      //   localStorage.removeItem('err_resp_status')
      //   await (savePtlYearSemester())
      // }
      if (
        (localStorage.getItem('resp_status')?.includes('201') ||
          localStorage.getItem('resp_status')?.includes('200')) &&
        !localStorage.getItem('err_resp_status')?.includes('400') &&
        !localStorage.getItem('err_resp_status')?.includes('401') &&
        !localStorage.getItem('err_resp_status')?.includes('500')
      ) {
        await getAllPtlYearSemestersByManP(take.value, page.value, keyword.value)
      }
    } catch (e) {
      ismodal.value = ''

      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await (savePtlYearSemester())
      } else {
        varcompnStore.showMessage('ไม่สามารถบันทึกข้อมูลปี/ภาคการศึกษาได้')
      }
    }
    varcompnStore.isLoading = false
  }

  async function editPtlYearSemester(ptlYearSemester: PtlYearSemester) {
    varcompnStore.isLoading = true
    editedPtlYearSemester.value = JSON.parse(JSON.stringify(ptlYearSemester))
    varcompnStore.isLoading = false
  }

  async function deletePtlYearSemester(ptlYearSemester: PtlYearSemester) {
    varcompnStore.isLoading = true
    try {
      ptlYearSemester.DELETE_USER = localStorage.getItem('nameonly')!
      await ptlYearSemesterService.deletePtlYearSemester(ptlYearSemester.YRS_ID!, ptlYearSemester)

      await getAllPtlYearSemestersByManP(take.value, page.value, keyword.value)
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await (deletePtlYearSemester(ptlYearSemester))
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถลบข้อมูลปี/ภาคการศึกษาได้')
      }
    }
    varcompnStore.isLoading = false
  }

  return {
    ptlYearSemesters,
    ptlYearSemester,
    getAllPtlYearSemesters,
    editedPtlYearSemester,
    savePtlYearSemester,
    editPtlYearSemester,
    deletePtlYearSemester,
    clearPtlYearSemester,
    getOnePtlYearSemester,
    typeAppointSemesters,
    yearths,
    ismodal,
    getAllPtlYearSemestersByManP,
    ptlYearSemestersManP,
    lastPage,
    page,
    take,
    keyword,
    total
  }
})
