import { ref } from 'vue'
import { defineStore } from 'pinia'
import type PtlGraduate from '@/types/PtlGraduate'
import { useVarcompnStore } from './varcompn'
import ptlGraduateService from '@/services/ptl-graduate'

export const usePtlGraduateStore = defineStore('ptlGraduate', () => {
  const varcompnStore = useVarcompnStore()
  const ptlGraduates = ref<PtlGraduate[]>([])
  const editedPtlGraduate = ref<PtlGraduate>({
    GRA_MAJOR: '',
    GRA_UNIVERSITYE: '',
    CTY_ID: '',
    GRA_YEAR: ''
  })

  function clearPtlGraduate() {
    editedPtlGraduate.value = {
      GRA_MAJOR: '',
      GRA_UNIVERSITYE: '',
      CTY_ID: '',
      GRA_YEAR: ''
    }
  }

  async function getAllPtlGraduates() {
    try {
      const res = await ptlGraduateService.getAllPtlGraduates()
      ptlGraduates.value = res.data
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await varcompnStore.redo(getAllPtlGraduates())
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลวุฒิการศึกษาทั้งหมดได้')
      }
    }
  }

  async function savePtlGraduate(LEC_ID: number) {
    try {
      if (editedPtlGraduate.value.GRA_ID) {
        editedPtlGraduate.value.UPDATE_USER = localStorage.getItem('nameonly')!
        editedPtlGraduate.value.UPDATE_PROG = 'PtlGraduate'

        await ptlGraduateService.updatePtlGraduate(
          editedPtlGraduate.value.GRA_ID,
          editedPtlGraduate.value
        )
      } else {
        editedPtlGraduate.value.CREATE_USER = localStorage.getItem('nameonly')!

        await ptlGraduateService.savePtlGraduate(LEC_ID, editedPtlGraduate.value)
      }

      // await getAllPtlGraduates()
      // if (localStorage.getItem('err_resp_status')?.includes('401')) {
      //   await varcompnStore.redo(savePtlGraduate(LEC_ID))
      // }
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await varcompnStore.redo(savePtlGraduate(LEC_ID))
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถบันทึกข้อมูลวุฒิการศึกษาได้')
      }
    }
  }

  function editPtlGraduate(ptlGraduate: PtlGraduate) {
    editedPtlGraduate.value = JSON.parse(JSON.stringify(ptlGraduate))
    editedPtlGraduate.value.LEC_ID = ptlGraduate.LECT!.LEC_ID
  }

  async function deletePtlGraduate(ptlGraduate: PtlGraduate) {
    try {
      ptlGraduate.DELETE_USER = localStorage.getItem('nameonly')!
      await ptlGraduateService.deletePtlGraduate(ptlGraduate.GRA_ID!, ptlGraduate)

      await getAllPtlGraduates()
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await varcompnStore.redo(deletePtlGraduate(ptlGraduate))
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถลบข้อมูลวุฒิการศึกษาได้')
      }
    }
  }

  return {
    ptlGraduates,
    getAllPtlGraduates,
    editedPtlGraduate,
    savePtlGraduate,
    editPtlGraduate,
    deletePtlGraduate,
    clearPtlGraduate
  }
})
