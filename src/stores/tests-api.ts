import { ref } from 'vue'
import { defineStore } from 'pinia'
import testApi from '@/services/test-api'
import { useVarcompnStore } from './varcompn'

export const useTestPtlStore = defineStore('test-ptl', () => {
  const gData = ref({
    name: '',
    email: ''
  })

  const mailSend = ref({
    send_system: '',
    send_to: '',
    send_cc: '',
    send_subject: '',
    send_message: ''
  })
  const resSend = ref('')

  const gFileList = ref({
    path: '',
    bucket: ''
  })

  const fileDel = ref({
    filePath: '',
    bucket: ''
  })
  const resDelFile = ref('')

  const addFile = ref<any & { files: File[] }>({
    path: '',
    fileName: '',
    bucket: '',
    originalExtension: '',
    files: []
  })
  const resAddFile = ref('')

  const gFile = ref({
    filePath: {},

    bucket: '',
    originalFileName: {}
  })
  const resGetFile = ref('')

  const getFilenameFromPut = ref('')
  const getFilenameFromGP = ref('')

  const varcompnStore = useVarcompnStore()

  const sendMail = async (
    send_system: string,
    send_to: string,
    send_cc: string,
    send_subject: string,
    send_message: string
  ): Promise<void> => {
    varcompnStore.isServiceApi = 's-api'
    try {
      const res = await testApi.sendMail(send_system, send_to, send_cc, send_subject, send_message)

      localStorage.setItem('ressend', JSON.stringify(res.data))
      resSend.value = JSON.parse(localStorage.getItem('ressend')!)
    } catch (err) {
      // console.log(err);
    }
  }

  const deleteFile = async (filePath: string): Promise<void> => {
    varcompnStore.isServiceApi = 's-api'
    try {
      const res = await testApi.deleteFile(filePath)

      localStorage.setItem('resfiledel', JSON.stringify(res.data))
      resDelFile.value = JSON.parse(localStorage.getItem('resfiledel')!)
    } catch (err) {
      // console.log(err);
    }
  }

  const putFile = async (
    fileName: string,
    originalExtension: string,
    files: File
  ): Promise<void> => {
    varcompnStore.isServiceApi = 's-api'
    try {
      const ext = ref()
      const originalExtension = ref('')
      if (files.name) {
        ext.value = files.name.split('.')

        originalExtension.value = ext.value[1]
      }

      const res = await testApi.putFile(fileName, originalExtension.value, files)

      getFilenameFromPut.value = res.data.result

      localStorage.setItem('resadd', JSON.stringify(res.data))
      resAddFile.value = JSON.parse(localStorage.getItem('resadd')!)
    } catch (err) {
      // console.log(err);
    }
  }

  const getFile = async (filePath: {}, originalFileName: {}): Promise<void> => {
    varcompnStore.isServiceApi = 's-api'
    try {
      const res = await testApi.getPublicFile(filePath, originalFileName)

      localStorage.setItem('resgetfile', JSON.stringify(res.data))
      resGetFile.value = JSON.parse(localStorage.getItem('resgetfile')!)
      getFilenameFromGP.value = res.data.result
    } catch (err) {
      // console.log(err);
    }
  }

  return {
    gData,
    mailSend,
    sendMail,
    resSend,
    gFileList,
    fileDel,
    deleteFile,
    resDelFile,
    addFile,
    putFile,
    resAddFile,
    gFile,
    getFile,
    resGetFile,
    getFilenameFromPut,
    getFilenameFromGP
  }
})
