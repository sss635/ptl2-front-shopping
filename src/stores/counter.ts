import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type DataListG from '@/types/TestDataListG'

export const useCounterStore = defineStore('counter', () => {
  const count = ref(0)
  const doubleCount = computed(() => count.value * 2)
  function increment() {
    count.value++
  }

  const numListG = ref(1)
  const editedDataListGs = ref<DataListG[]>([])
  const dataListGs = ref<DataListG[]>([])
  const dataListG = ref<DataListG>({
    emailt: "",
    atcOne: "",
    atcMulti: []
  })
  dataListGs.value.push(dataListG.value)

  async function saveDataListG() {
    try {
      editedDataListGs.value = []
      dataListGs.value.forEach((element) => {
        editedDataListGs.value.push({
          emailt: element.tempemailt!,
          atcOne: element.tempatcOne!,
          atcMulti: element.tempatcMulti!
        });
      });
    } catch (e) {
      console.log(e);
    }
  }


  return { count, doubleCount, increment, numListG, dataListG, dataListGs, saveDataListG, editedDataListGs }
})
