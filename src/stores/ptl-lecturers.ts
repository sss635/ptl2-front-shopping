import loginService from '@/services/login'
import { ref, watch } from 'vue'
import { defineStore } from 'pinia'
import type PtlLecturer from '@/types/PtlLecturer'
import { useVarcompnStore } from './varcompn'
import ptlLecturerService from '@/services/ptl-lecturer'
import queryService from '@/services/query'
import { useTestPtlStore } from './tests-api'
import router from '@/router'

export const usePtlLecturerStore = defineStore('ptlLecturer', () => {
  const varcompnStore = useVarcompnStore()
  const testPtls = useTestPtlStore()
  const ptlLecturers = ref<PtlLecturer[]>([])
  const ptlLecturersManP = ref<PtlLecturer[]>([])
  const ptlLecturersY = ref<PtlLecturer[]>([])
  const rPLectByTerms = ref<{}[]>([])
  const listQ = ref<number[]>([])
  const editedPtlLecturer = ref<PtlLecturer & { filesEdu: File[] } & { filesPhot: File[] }>({
    // PRF_ID: '',
    PRF_NAMETH: '',
    LEC_NAME: '',
    LEC_NAME_ENG: '',
    // LEC_MIDDLE_NAME: '',
    // LEC_MIDDLE_NAME_ENG: '',
    LEC_SURNAME: '',
    LEC_SURNAME_ENG: '',
    NNT_ID: '',
    LEC_CARD_ID: '',
    LEC_ADDRESS_NUMBER: '',
    LEC_ADDRESS_ALLEY: '',
    LEC_ADDRESS_ROAD: '',
    TAM_ID: '',
    AMP_ID: '',
    AMP_ZIPCODE: '',
    PROV_ID: '',
    LEC_PHONE: '',
    LEC_EMAIL: '',
    LEC_CURRENT_POSITION: '',
    LEC_WORKPLACE: '',
    LEC_ACADEMIC_WORKS: '',
    LEC_TALENT: '',

    GRA_LISTS: [],
    filesEdu: [],
    filesPhot: []
  })
  const lastPage = ref(1)
  const page = ref(1)
  const take = ref(10)
  const keyword = ref('')
  const total = ref(0)
  const boo = ref('')

  watch(keyword, async (newKeyword, oldKeyword) => {
    await getAllPtlLecturersByManP(take.value, page.value, keyword.value)
  })

  watch(lastPage, async (newLastPage, oldLastPage) => {
    if (newLastPage < page.value) {
      page.value = 1
      await getAllPtlLecturersByManP(take.value, page.value, keyword.value)
    }
  })

  function clearPtlLecturer() {
    editedPtlLecturer.value = {
      // PRF_ID: '',
      PRF_NAMETH: '',
      LEC_NAME: '',
      LEC_NAME_ENG: '',
      // LEC_MIDDLE_NAME: '',
      // LEC_MIDDLE_NAME_ENG: '',
      LEC_SURNAME: '',
      LEC_SURNAME_ENG: '',
      NNT_ID: '',
      LEC_CARD_ID: '',
      LEC_ADDRESS_NUMBER: '',
      LEC_ADDRESS_ALLEY: '',
      LEC_ADDRESS_ROAD: '',
      TAM_ID: '',
      AMP_ID: '',
      AMP_ZIPCODE: '',
      PROV_ID: '',
      LEC_PHONE: '',
      LEC_EMAIL: '',
      LEC_CURRENT_POSITION: '',
      LEC_WORKPLACE: '',
      LEC_ACADEMIC_WORKS: '',
      LEC_TALENT: '',
      GRA_LISTS: [],
      filesEdu: [],
      filesPhot: []
    }
  }

  async function getAllPtlLecturers() {
    varcompnStore.isLoading = true
    try {
      const res = await ptlLecturerService.getAllPtlLecturers()
      ptlLecturers.value = res.data
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(getAllPtlLecturers())
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลอาจารย์พิเศษทั้งหมดได้')
      }
    }
    varcompnStore.isLoading = false
  }

  async function getAllPtlLecturersByManP(take: number, page: number, keyword: string) {
    try {
      const res = await ptlLecturerService.getAllPtlLecturersByManP(take, page, keyword)
      ptlLecturersManP.value = res.data?.data
      lastPage.value = res.data.lastPage
      total.value = res.data.count
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(getAllPtlLecturersByManP(take, page, keyword))
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลอาจารย์พิเศษทั้งหมดได้')
      }
    }
  }

  async function getAllPtlLecturersByCF() {
    varcompnStore.isLoading = true
    try {
      const res = await ptlLecturerService.getAllPtlLecturersByCF()
      ptlLecturersY.value = res.data
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(getAllPtlLecturersByCF())
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลอาจารย์พิเศษทั้งหมดที่ confirm แล้วได้')
      }
    }
    varcompnStore.isLoading = false
  }

  async function getOnePtlLecturer(id: number) {
    varcompnStore.isLoading = true
    try {
      const res = await ptlLecturerService.getOnePtlLecturer(id)

      editedPtlLecturer.value = res.data
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(getOnePtlLecturer(id))
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลอาจารย์พิเศษนี้ได้')
      }
    }
    varcompnStore.isLoading = false
  }

  async function getRPLectByTerm(year: string, semester: number) {
    // varcompnStore.isLoading = true
    try {
      const res = await queryService.getRPLectByTerm(year, semester)
      const tempres = res.data
      rPLectByTerms.value = []
      let q = 1
      listQ.value = []
      for (let i = 0; i < tempres.length; i++) {
        if (i == 0 || (i > 0 && tempres[i].FAC_NAMETH != tempres[i - 1].FAC_NAMETH)) {
          q = 1
        }
        if (
          i == 0 ||
          (i > 0 &&
            (tempres[i].FAC_NAMETH != tempres[i - 1].FAC_NAMETH ||
              tempres[i].LEC_CARD_ID != tempres[i - 1].LEC_CARD_ID))
        ) {
          const { start_date, end_date } = varcompnStore.proveSEdateforRP(
            tempres[i].YRS_START_DATE.substring(8, 10),
            tempres[i].YRS_START_DATE.substring(5, 7),
            tempres[i].YRS_START_DATE.substring(0, 4),
            tempres[i].YRS_END_DATE.substring(8, 10),
            tempres[i].YRS_END_DATE.substring(5, 7),
            tempres[i].YRS_END_DATE.substring(0, 4),
            tempres[i].YRS_START_DATE.substring(11, 13),
            tempres[i].YRS_END_DATE.substring(11, 13)
          )
          tempres[i].YRS_START_DATE = start_date
          tempres[i].YRS_END_DATE = end_date
          rPLectByTerms.value.push(tempres[i])
          listQ.value.push(q)
          q++
        }
      }
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(getRPLectByTerm(year, semester))
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลอาจารย์พิเศษทั้งหมดที่เลือกได้')
      }
    }
    // varcompnStore.isLoading = false
  }

  const assertThaiId = (thaiId: string): boolean => {
    // console.log(thaiId)
    boo.value = ''
    if (thaiId == '') {
      // console.log('null')
      varcompnStore.message = 'กรุณากรอกเลขประจำตัวประชาชน'
      return false
    }
    const m = thaiId.match(/(\d{12})(\d)/)
    if (!m) {
      // console.warn('Bad input from user, invalid thaiId=', thaiId)
      varcompnStore.showMessage('กรุณาตรวจสอบความถูกต้องของเลขประจำตัวประชาชน')
      return false
      // throw new Error('thai-id-must-be-13-digits')
    }
    const digits = m[1].split('')
    const sum = digits.reduce((total: number, digit: string, i: number) => {
      return total + (13 - i) * +digit
    }, 0)
    const lastDigit = `${(11 - (sum % 11)) % 10}`
    const inputLastDigit = m[2]
    // console.log(lastDigit, inputLastDigit)
    if (lastDigit !== inputLastDigit) {
      // console.warn('Bad input from user2, invalid checksum thaiId=', thaiId)
      varcompnStore.showMessage('กรุณาตรวจสอบความถูกต้องของเลขประจำตัวประชาชน')
      // throw new Error('thai-id-checksum-mismatched')
      return false
    }
    return true
  }

  async function checkPer(thaiId: string, type: string) {
    const result = await loginService.getPersonFromCitizenID(thaiId)
    if (result.data.status == 'success') {
      // if(thaiId=='2066652104934'){
      // boo.value = 'notPer';
      // }else{
      boo.value = 'isPer'
      // }
    } else if (result.data.status == 'fail') {
      boo.value = 'notPer'
      localStorage.removeItem('err_resp_status')
    }
    // console.log(boo.value)
    if (boo.value == 'isPer') {
      // console.log('showerr')
      if (type == 'cid') {
        // varcompnStore.showMessage('เลขประจำตัวประชาชนนี้เป็นบุคคลกรในมหาวิทยาลัย')
        varcompnStore.message = 'เลขประจำตัวประชาชนนี้เป็นบุคคลกรในมหาวิทยาลัย'
      }
      if (type == 'pass') {
        // varcompnStore.showMessage('หมายเลขหนังสือเดินทางนี้เป็นบุคคลกรในมหาวิทยาลัย')
        varcompnStore.message = 'หมายเลขหนังสือเดินทางนี้เป็นบุคคลกรในมหาวิทยาลัย'
      }
      return false
    } else if (boo.value == 'notPer') {
      return true
    }
  }

  const assertPassId = (passId: string): boolean => {
    if (passId == '') {
      varcompnStore.message = 'กรุณากรอกหมายเลขหนังสือเดินทาง'
      return false
    }
    // console.log('st'+passId)
    const m = passId.match(/^[a-zA-Z0-9]{8,20}$/)
    if (!m) {
      varcompnStore.showMessage('กรุณาตรวจสอบความถูกต้องของหมายเลขหนังสือเดินทาง')
      return false
    }

    return true
  }

  async function savePtlLecturer(filesEdu: File, filesPhot: File) {
    varcompnStore.isLoading = true
    try {
      if (filesEdu && filesEdu.name) {
        const ext = filesEdu.name.split('.')
        const originalExtension = ext[1]
        await testPtls.putFile('', originalExtension, filesEdu)
        const resEdu = testPtls.getFilenameFromPut

        if (
          editedPtlLecturer.value.LEC_EDUCATION_FILE_NAME != '' &&
          editedPtlLecturer.value.LEC_EDUCATION_FILE_NAME != null
        ) {
          await testPtls.deleteFile(editedPtlLecturer.value.LEC_EDUCATION_FILE_NAME!)
        }
        editedPtlLecturer.value.LEC_EDUCATION_FILE_NAME = resEdu
      }
      if (filesPhot && filesPhot.name) {
        const ext = filesPhot.name.split('.')
        const originalExtension = ext[1]
        await testPtls.putFile('', originalExtension, filesPhot)
        const resPhot = testPtls.getFilenameFromPut

        if (
          editedPtlLecturer.value.LEC_PHOTO_FILE_NAME != '' &&
          editedPtlLecturer.value.LEC_PHOTO_FILE_NAME != null
        ) {
          await testPtls.deleteFile(editedPtlLecturer.value.LEC_PHOTO_FILE_NAME!)
        }
        editedPtlLecturer.value.LEC_PHOTO_FILE_NAME = resPhot
      }

      if (editedPtlLecturer.value.LEC_ID) {
        editedPtlLecturer.value.UPDATE_USER = localStorage.getItem('nameonly')!
        editedPtlLecturer.value.UPDATE_PROG = 'PtlLecturer'
        await ptlLecturerService.updatePtlLecturer(
          editedPtlLecturer.value.LEC_ID,
          editedPtlLecturer.value
        )
      } else {
        editedPtlLecturer.value.CREATE_USER = localStorage.getItem('nameonly')!
        await ptlLecturerService.savePtlLecturer(editedPtlLecturer.value)
      }

      // console.log(localStorage.getItem('resp_status'))
      if (
        (localStorage.getItem('resp_status')?.includes('201') ||
          localStorage.getItem('resp_status')?.includes('200')) &&
        !localStorage.getItem('err_resp_status')?.includes('400') &&
        !localStorage.getItem('err_resp_status')?.includes('401') &&
        !localStorage.getItem('err_resp_status')?.includes('500')
      ) {
        await getAllPtlLecturersByManP(take.value, page.value, keyword.value)
        const ch = await ptlLecturerService.getOnePtlLecturerByCid(
          editedPtlLecturer.value.LEC_CARD_ID
        )
        // console.log(ch)
        if (ch != undefined) {
          router.replace('/spe-instructor-manage')
        }

        varcompnStore.pathsIns = '/spe-instructor-manage'
      }
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(savePtlLecturer(filesEdu, filesPhot))
      } else {
        varcompnStore.showMessage('ไม่สามารถบันทึกข้อมูลอาจารย์พิเศษได้')
      }
    }
    varcompnStore.isLoading = false
  }

  async function savePtlLecturerCF(ptlLecturer: PtlLecturer) {
    varcompnStore.isLoading = true
    try {
      if (ptlLecturer.LEC_ID) {
        ptlLecturer.UPDATE_USER = localStorage.getItem('nameonly')!
        ptlLecturer.UPDATE_PROG = 'Confirm PtlLecturer'
        await ptlLecturerService.updatePtlLecturer(ptlLecturer.LEC_ID, ptlLecturer)
      }
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(savePtlLecturerCF(ptlLecturer))
      }
      if (
        (localStorage.getItem('resp_status')?.includes('201') ||
          localStorage.getItem('resp_status')?.includes('200')) &&
        !localStorage.getItem('err_resp_status')?.includes('400') &&
        !localStorage.getItem('err_resp_status')?.includes('401') &&
        !localStorage.getItem('err_resp_status')?.includes('500')
      ) {
        await getAllPtlLecturersByManP(take.value, page.value, keyword.value)
      }
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(savePtlLecturerCF(ptlLecturer))
      } else {
        varcompnStore.showMessage('ไม่สามารถบันทึกการตรวจสอบข้อมูลอาจารย์พิเศษได้')
      }
    }
    varcompnStore.isLoading = false
  }

  async function savePtlLecturerCFFromEmail() {
    varcompnStore.isLoading = true
    try {
      if (editedPtlLecturer.value.LEC_ID) {
        editedPtlLecturer.value.UPDATE_USER =
          editedPtlLecturer.value.PRF_NAMETH +
          editedPtlLecturer.value.LEC_NAME +
          ' ' +
          editedPtlLecturer.value.LEC_SURNAME
        editedPtlLecturer.value.UPDATE_PROG = 'Confirm ข้อมูลอาจารย์พิเศษ From Email'
        await ptlLecturerService.updatePtlLecturerFromEmail(
          editedPtlLecturer.value.LEC_ID,
          editedPtlLecturer.value
        )
      }
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(savePtlLecturerCFFromEmail())
      } else {
        varcompnStore.showMessage('ไม่สามารถบันทึกการเปลี่ยนแปลงได้')
      }
    }
    varcompnStore.isLoading = false
  }

  async function updatePtlLecturerCreateUser() {
    varcompnStore.isLoading = true
    try {
      if (editedPtlLecturer.value.LEC_ID) {
        editedPtlLecturer.value.UPDATE_USER = localStorage.getItem('nameonly')!
        editedPtlLecturer.value.UPDATE_PROG = 'สร้างบัญชีผู้ใช้'
        await ptlLecturerService.updatePtlLecturerCreateUser(
          editedPtlLecturer.value.LEC_ID,
          editedPtlLecturer.value
        )
      }
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(updatePtlLecturerCreateUser())
      } else {
        varcompnStore.showMessage('ไม่สามารถสร้างบัญชีผู้ใช้ได้')
      }
    }
    varcompnStore.isLoading = false
  }

  async function updateByDeleteFile(type: string) {
    varcompnStore.isLoading = true
    try {
      if (type === 'photo') {
        await testPtls.deleteFile(editedPtlLecturer.value.LEC_PHOTO_FILE_NAME!)
        editedPtlLecturer.value.LEC_PHOTO_FILE_NAME = ''
      }
      if (type === 'docedu') {
        await testPtls.deleteFile(editedPtlLecturer.value.LEC_EDUCATION_FILE_NAME!)
        editedPtlLecturer.value.LEC_EDUCATION_FILE_NAME = ''
      }
      editedPtlLecturer.value.UPDATE_USER = localStorage.getItem('nameonly')!
      editedPtlLecturer.value.UPDATE_PROG = 'PtlLecturer'
      await ptlLecturerService.updatePtlLecturer(
        editedPtlLecturer.value.LEC_ID!,
        editedPtlLecturer.value
      )
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(updateByDeleteFile(type))
      } else {
        varcompnStore.showMessage('ไม่สามารถอัปเดตข้อมูลอาจารย์พิเศษด้วยไฟล์ได้')
      }
    }
    varcompnStore.isLoading = false
  }

  async function editPtlLecturer(ptlLecturer: PtlLecturer, enc_lecid: string) {
    router.replace({
      path: '/spe-instructor-manage/spe-instructor-form',

      query: { type: 'edit', lec_id: enc_lecid }
    })
    varcompnStore.pathsIns = '/spe-instructor-manage'
  }

  async function deletePtlLecturer(ptlLecturer: PtlLecturer) {
    try {
      ptlLecturer.DELETE_USER = localStorage.getItem('nameonly')!
      await ptlLecturerService.deletePtlLecturer(ptlLecturer.LEC_ID!, ptlLecturer)
      if (
        (localStorage.getItem('resp_status')?.includes('201') ||
          localStorage.getItem('resp_status')?.includes('200')) &&
        !localStorage.getItem('err_resp_status')?.includes('400') &&
        !localStorage.getItem('err_resp_status')?.includes('401') &&
        !localStorage.getItem('err_resp_status')?.includes('500')
      ) {
        await getAllPtlLecturersByManP(take.value, page.value, keyword.value)
      }
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(deletePtlLecturer(ptlLecturer))
      } else {
        varcompnStore.showMessage('ไม่สามารถลบข้อมูลอาจารย์พิเศษได้')
      }
    }
  }

  return {
    ptlLecturers,
    getAllPtlLecturers,
    editedPtlLecturer,
    savePtlLecturer,
    editPtlLecturer,
    deletePtlLecturer,
    clearPtlLecturer,
    getOnePtlLecturer,
    ptlLecturersY,
    getAllPtlLecturersByCF,
    updateByDeleteFile,

    getAllPtlLecturersByManP,
    ptlLecturersManP,
    lastPage,
    page,
    take,
    keyword,
    savePtlLecturerCF,
    savePtlLecturerCFFromEmail,
    assertThaiId,
    assertPassId,
    updatePtlLecturerCreateUser,
    total,
    checkPer,
    getRPLectByTerm,
    rPLectByTerms,
    listQ
  }
})
