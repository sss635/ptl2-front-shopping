import { ref } from 'vue'
import { defineStore } from 'pinia'
import type PtlFacultyPersonPermission from '@/types/PtlFacPermission'
import { useVarcompnStore } from './varcompn'
import ptlFacultyPersonPermissionService from '@/services/ptl-fac-permission'

export const usePtlFacultyPersonPermissionStore = defineStore('ptlFacultyPersonPermission', () => {
  const varcompnStore = useVarcompnStore()
  const ptlFacultyPersonPermissions = ref<PtlFacultyPersonPermission[]>([])
  const ptlFacultyPersonPermissionsByPer = ref<PtlFacultyPersonPermission[]>([])
  const editedPtlFacultyPersonPermission = ref<PtlFacultyPersonPermission>({
    FAC_ID: '',
    FAC_NAME: '',
    PSN_ID: '',
    PRF_NAMETH: '',
    PSN_FNAMETH: '',
    PSN_LNAMETH: '',
    ADM_NAMETH: '',
    USLOGIN: ''
  })

  function clearPtlFacultyPersonPermission() {
    editedPtlFacultyPersonPermission.value = {
      FAC_ID: '',
      FAC_NAME: '',
      PSN_ID: '',
      PRF_NAMETH: '',
      PSN_FNAMETH: '',
      PSN_LNAMETH: '',
      ADM_NAMETH: '',
      USLOGIN: ''
    }
  }

  async function getAllPtlFacultyPersonPermissions() {
    try {
      const res = await ptlFacultyPersonPermissionService.getAllPtlFacultyPersonPermissions()
      ptlFacultyPersonPermissions.value = res.data
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(getAllPtlFacultyPersonPermissions())
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลสิทธิ์ส่วนงานทั้งหมดได้')
      }
    }
  }

  async function getAllPtlFacultyPersonPermissionByPerson(psn_id: string) {
    try {
      const res =
        await ptlFacultyPersonPermissionService.getAllPtlFacultyPersonPermissionByPerson(psn_id)
      ptlFacultyPersonPermissionsByPer.value = res.data
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(getAllPtlFacultyPersonPermissionByPerson(psn_id))
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลสิทธิ์ส่วนงานของผู้ใช้ได้')
      }
    }
  }

  async function getOnePtlFacultyPersonPermission(fac_id: string, psn_id: string) {
    try {
      const res = await ptlFacultyPersonPermissionService.getOnePtlFacultyPersonPermission(
        fac_id,
        psn_id
      )

      editedPtlFacultyPersonPermission.value = res.data
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(getOnePtlFacultyPersonPermission(fac_id, psn_id))
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลสิทธิ์ส่วนงานของผู้ใช้ได้')
      }
    }
  }

  async function savePtlFacultyPersonPermission() {
    try {
      editedPtlFacultyPersonPermission.value.CREATE_USER = localStorage.getItem('nameonly')!
      await ptlFacultyPersonPermissionService.savePtlFacultyPersonPermission(
        editedPtlFacultyPersonPermission.value
      )

      await getAllPtlFacultyPersonPermissions()
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(savePtlFacultyPersonPermission())
      } else {
        varcompnStore.showMessage('ไม่สามารถบันทึกข้อมูลสิทธิ์ส่วนงานได้')
      }
    }
  }

  async function editPtlFacultyPersonPermission(
    ptlFacultyPersonPermission: PtlFacultyPersonPermission
  ) {
    editedPtlFacultyPersonPermission.value = ptlFacultyPersonPermission
    editedPtlFacultyPersonPermission.value.FAC_ID = ptlFacultyPersonPermission.FAC_ID
  }

  async function deletePtlFacultyPersonPermission(
    ptlFacultyPersonPermission: PtlFacultyPersonPermission
  ) {
    try {
      ptlFacultyPersonPermission.DELETE_USER = localStorage.getItem('nameonly')!
      await ptlFacultyPersonPermissionService.deletePtlFacultyPersonPermission(
        ptlFacultyPersonPermission.FAC_ID!,
        ptlFacultyPersonPermission.PSN_ID!,
        ptlFacultyPersonPermission
      )

      await getAllPtlFacultyPersonPermissions()
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(deletePtlFacultyPersonPermission(ptlFacultyPersonPermission))
      } else {
        varcompnStore.showMessage('ไม่สามารถลบข้อมูลสิทธิ์ส่วนงานได้')
      }
    }
  }

  return {
    ptlFacultyPersonPermissions,
    ptlFacultyPersonPermissionsByPer,
    getAllPtlFacultyPersonPermissions,
    editedPtlFacultyPersonPermission,
    savePtlFacultyPersonPermission,
    editPtlFacultyPersonPermission,
    deletePtlFacultyPersonPermission,
    clearPtlFacultyPersonPermission,
    getOnePtlFacultyPersonPermission,
    getAllPtlFacultyPersonPermissionByPerson
  }
})
