import { ref } from 'vue'
import { defineStore } from 'pinia'
import type PtlLogEmail from '@/types/PtlLogEmail'
import { useVarcompnStore } from './varcompn'
import ptlLogEmailService from '@/services/ptl-log-email'

export const usePtlLogEmailStore = defineStore('ptlLogEmail', () => {
  const varcompnStore = useVarcompnStore()
  const ptlLogEmails = ref<PtlLogEmail[]>([])
  const editedPtlLogEmail = ref<PtlLogEmail>({
    EMAIL_RECEIVER: '',
    EMAIL_MESSAGE: ''
  })

  function clearPtlLogEmail() {
    editedPtlLogEmail.value = {
      EMAIL_RECEIVER: '',
      EMAIL_MESSAGE: ''
    }
  }

  async function getAllPtlLogEmails() {
    try {
      const res = await ptlLogEmailService.getAllPtlLogEmails()
      ptlLogEmails.value = res.data
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await varcompnStore.redo(getAllPtlLogEmails())
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลประวัติการส่งอีเมลทั้งหมดได้')
      }
    }
  }

  async function getOnePtlLogEmail(id: number) {
    try {
      const res = await ptlLogEmailService.getOnePtlLogEmail(id)

      editedPtlLogEmail.value = res.data
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await varcompnStore.redo(getOnePtlLogEmail(id))
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลประวัติการส่งอีเมลได้')
      }
    }
  }

  async function savePtlLogEmail() {
    try {
      editedPtlLogEmail.value.CREATE_USER = localStorage.getItem('nameonly')!
      await ptlLogEmailService.savePtlLogEmail(editedPtlLogEmail.value)

      await getAllPtlLogEmails()
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        //   await varcompnStore.redo(savePtlLogEmail())
        localStorage.removeItem('err_resp_status')
      } else {
        varcompnStore.showMessage('ไม่สามารถบันทึกข้อมูลประวัติการส่งอีเมลได้')
      }
    }
  }

  return {
    ptlLogEmails,
    getAllPtlLogEmails,
    editedPtlLogEmail,
    savePtlLogEmail,
    clearPtlLogEmail,
    getOnePtlLogEmail
  }
})
