import { ref } from 'vue'
import { defineStore } from 'pinia'
import type PtlAppointLogStatus from '@/types/PtlAppointLogStatus'
import { useVarcompnStore } from './varcompn'
import ptlAppointLogStatusService from '@/services/ptl-appointlog'

export const usePtlAppointLogStatusStore = defineStore('ptlAppointLogStatus', () => {
  const varcompnStore = useVarcompnStore()
  const ptlAppointLogStatuss = ref<PtlAppointLogStatus[]>([])
  const editedPtlAppointLogStatus = ref<PtlAppointLogStatus>({
    APPLS_NOTE: ''
  })

  function clearPtlAppointLogStatus() {
    editedPtlAppointLogStatus.value = {
      APPLS_NOTE: ''
    }
  }

  async function getAllPtlAppointLogStatuss() {
    try {
      const res = await ptlAppointLogStatusService.getAllPtlAppointLogStatuses()
      ptlAppointLogStatuss.value = res.data
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(getAllPtlAppointLogStatuss())
      } else {
        varcompnStore.showMessage(
          'ไม่สามารถดึงข้อมูลประวัติการอัปเดตสถานะคำขอเสนอแต่งตั้งทั้งหมดได้'
        )
      }
    }
  }

  async function getOnePtlAppointLogStatus(id: number) {
    try {
      const res = await ptlAppointLogStatusService.getOnePtlAppointLogStatus(id)

      editedPtlAppointLogStatus.value = res.data
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(getOnePtlAppointLogStatus(id))
      } else {
        varcompnStore.showMessage('ไม่สามารถดึงข้อมูลประวัติการอัปเดตสถานะคำขอเสนอแต่งตั้งได้')
      }
    }
  }

  async function savePtlAppointLogStatus(APP_ID: number) {
    try {
      editedPtlAppointLogStatus.value.CREATE_USER = localStorage.getItem('nameonly')!
      await ptlAppointLogStatusService.savePtlAppointLogStatus(
        APP_ID,
        editedPtlAppointLogStatus.value
      )

      await getAllPtlAppointLogStatuss()
    } catch (e) {
      if (localStorage.getItem('err_resp_status')?.includes('401')) {
        localStorage.removeItem('err_resp_status')
        //   await varcompnStore.redo(savePtlAppointLogStatus(APP_ID))
      } else {
        varcompnStore.showMessage('ไม่สามารถบันทึกข้อมูลประวัติการอัปเดตสถานะคำขอเสนอแต่งตั้งได้')
      }
    }
  }

  return {
    ptlAppointLogStatuss,
    getAllPtlAppointLogStatuss,
    editedPtlAppointLogStatus,
    savePtlAppointLogStatus,
    clearPtlAppointLogStatus,
    getOnePtlAppointLogStatus
  }
})
