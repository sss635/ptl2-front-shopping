import type PtlAppoint from './PtlAppoint'
import type PtlAppointCourseTeacher from './PtlAppointCourseTeacher'
import type PtlLecturer from './PtlLecturer'
import type PtlYearSemester from './PtlYearSemester'

export default interface PtlAppointLecturer {
  APPL_ID?: number
  APP_ID?: number
  APPOINT?: PtlAppoint
  LEC_ID?: number
  LECT?: PtlLecturer
  YRS_ID?: number
  YEARSEMESTER?: PtlYearSemester
  CURR_ID: string
  CURR_YEAR: string
  CURR_NAMETH: string
  CURR_NAMEEN: string
  CURR_TYPE_NAME?: string
  APPL_REASON_NOAPPROVE?: string
  CURR_STATUS: string
  CURR_STATUS_NAME: string
  COU_CODE: string
  COU_YEAR: string
  COU_NAMETH: string
  COU_NAMEEN: string
  COU_CREDIT1: string
  COU_CREDIT2: string
  COU_CREDIT3: string
  COU_CREDIT4: string
  APPL_GROUP: string
  APPL_HOUR: number
  APPL_PERCENT: number
  APPL_YEAR: string
  APPL_SEMESTER: number
  LEV_ID: number
  LEV_NAMETH: string
  APPL_RATIO: string
  APPL_REASON: string
  APPL_STATUS?: string

  CREATE_USER?: string
  UPDATE_USER?: string
  UPDATE_PROG?: string
  DELETE_USER?: string

  APPCOURSE_LISTS?: PtlAppointCourseTeacher[]
}
