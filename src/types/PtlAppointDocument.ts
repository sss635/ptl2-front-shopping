import type PtlAppoint from './PtlAppoint'

export default interface PtlAppointDocument {
  APPD_ID?: number
  APP_ID?: number
  APPOINT?: PtlAppoint
  APPD_NAME: string
  APPD_FILE_NAME: string
  APPD_TYPE: number

  CREATE_USER?: string
  UPDATE_USER?: string
  UPDATE_PROG?: string
  DELETE_USER?: string
}
