import type PtlAppointLecturer from './PtlAppointLecturer'

export default interface PtlAppointCourseTeacher {
  APPL_ID?: number
  APPLECT?: PtlAppointLecturer
  PSN_ID?: string
  PRF_NAMETH?: string
  PSN_FNAMETH?: string
  PSN_LNAMETH?: string

  CREATE_USER?: string
  UPDATE_USER?: string
  UPDATE_PROG?: string
  DELETE_USER?: string
}
