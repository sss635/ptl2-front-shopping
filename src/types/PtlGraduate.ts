import type PtlLecturer from './PtlLecturer'

export default interface PtlGraduate {
  GRA_ID?: number
  LEC_ID?: number
  LECT?: PtlLecturer
  GRA_MAJOR: string
  GRA_UNIVERSITYE: string
  CTY_ID: string
  CTY_NAMETH?: string //for show pdf
  CTY?: {}
  GRA_YEAR: string
  GRA_SEQUENCE?: number
  CREATE_USER?: string
  UPDATE_USER?: string
  UPDATE_PROG?: string
  DELETE_USER?: string
}
