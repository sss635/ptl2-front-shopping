import type PtlAppoint from './PtlAppoint'

export default interface PtlAppointLogStatus {
  APPLS_ID?: number
  APP_ID?: number
  APPOINT?: PtlAppoint
  APP_STATUS?: number
  APPLS_DATE?: Date
  APPLS_NOTE?: string

  CREATE_USER?: string
  UPDATE_USER?: string
  UPDATE_PROG?: string
  DELETE_USER?: string
}
