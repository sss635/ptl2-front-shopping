import type PtlAppointLecturer from './PtlAppointLecturer'

export default interface PtlYearSemester {
  YRS_ID?: number
  YRS_YEAR: string
  YRS_SEMESTER: number
  YRS_START_DATE?: Date
  YRS_END_DATE?: Date
  CREATE_USER?: string
  UPDATE_USER?: string
  UPDATE_PROG?: string
  DELETE_USER?: string

  APPLECT_LISTS?: PtlAppointLecturer[]
}
