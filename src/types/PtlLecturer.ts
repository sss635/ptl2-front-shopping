import type PtlAppointLecturer from './PtlAppointLecturer'
import type PtlGraduate from './PtlGraduate'

export default interface PtlLecturer {
  LEC_ID?: number
  PRF_ID?: string
  PRF_NAMETH: string //for show pdf
  LEC_NAME: string
  LEC_NAME_ENG?: string
  LEC_MIDDLE_NAME?: string
  LEC_MIDDLE_NAME_ENG?: string
  LEC_SURNAME: string
  LEC_SURNAME_ENG?: string
  NNT_ID: string
  NNT_NAMETH?: string //for show pdf
  LEC_CARD_ID: string
  LEC_TYPE_PER?: number
  LEC_BIRTHDATE?: Date
  // LEC_BIRTHDATE: Date;
  LEC_AGE_YEAR?: number //for show pdf
  LEC_AGE_MONTH?: number //for show pdf
  LEC_ADDRESS_NUMBER?: string
  LEC_ADDRESS_ALLEY?: string
  LEC_ADDRESS_ROAD?: string
  TAM_ID: string
  TAM_NAMETH?: string //for show pdf
  AMP_ID: string
  AMP_NAMETH?: string //for show pdf
  AMP_ZIPCODE: string
  PROV_ID: string
  PROV_NAMETH?: string //for show pdf
  LEC_PHONE: string
  LEC_EMAIL: string
  LEC_CURRENT_POSITION: string
  LEC_WORKPLACE: string
  LEC_ACADEMIC_WORKS?: string
  LEC_TALENT?: string
  LEC_EDUCATION_FILE_NAME?: string
  LEC_PHOTO_FILE_NAME?: string
  LEC_DATA_CONFIRM?: string
  LEC_DATA_STATUS?: string
  LEC_DATA_REASON?: string
  LEC_START_DATE?: Date
  LEC_EXPIRE_DATE?: Date
  LEC_USERNAME?: string
  LEC_USERNAME_CREATE?: Date
  LEC_STATUS_CREATE?: string
  CREATE_USER?: string
  UPDATE_USER?: string
  UPDATE_PROG?: string
  DELETE_USER?: string

  GRA_LISTS?: PtlGraduate[]
  APPLECT_LISTS?: PtlAppointLecturer[]
}
