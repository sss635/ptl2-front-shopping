export default interface PtlFacultyPersonPermission {
  FAC_ID: string
  FAC_NAME: string
  PSN_ID: string
  PRF_NAMETH: string
  PSN_FNAMETH: string
  PSN_LNAMETH: string
  ADM_NAMETH: string
  USLOGIN: string

  CREATE_USER?: string
  UPDATE_USER?: string
  UPDATE_PROG?: string
  DELETE_USER?: string
}
