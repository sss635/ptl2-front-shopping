import type PtlAppointDocument from './PtlAppointDocument'
import type PtlAppointLecturer from './PtlAppointLecturer'
import type PtlAppointLogStatus from './PtlAppointLogStatus'

export default interface PtlAppoint {
  APP_ID?: number
  YRS_ID?: number
  FAC_ID: string
  FAC_NAMETH: string
  APP_NUMBER?: string
  APP_STATUS: number
  APP_OFFC_EMAIL: string

  CREATE_USER?: string
  UPDATE_USER?: string
  UPDATE_PROG?: string
  DELETE_USER?: string

  LOGSTS_LISTS?: PtlAppointLogStatus[]
  DOC_LISTS?: PtlAppointDocument[]
  APPLECT_LISTS?: PtlAppointLecturer[]
}
