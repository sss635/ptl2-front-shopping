export default interface PtlAppointLogStatus {
  EMAIL_ID?: number
  EMAIL_RECEIVER?: string
  EMAIL_MESSAGE?: string

  CREATE_USER?: string
  UPDATE_USER?: string
  UPDATE_PROG?: string
  DELETE_USER?: string
}
