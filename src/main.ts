// import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

// Vuetify
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import * as components from 'vuetify/components'
import * as directives from 'vuetify/directives'
import { aliases, mdi } from 'vuetify/iconsets/mdi-svg'
// snackbar
import { mdiAlertCircleOutline } from '@mdi/js'
// login
import { mdiAccount, mdiLock, mdiEye, mdiEyeOff } from '@mdi/js'
// Dropdown มุมบนขวา + 'mdiAccount, '
import { mdiSwapHorizontal, mdiFile, mdiPower, mdiChevronDown } from '@mdi/js'
// tools + 'mdiEye,
import { mdiNoteEditOutline, mdiTrashCanOutline, mdiCardAccountDetails } from '@mdi/js'
// table
import {
  mdiAccountOutline,
  mdiInboxMultipleOutline,
  mdiSend,
  mdiSendClock,
  mdiCursorDefault,
  mdiClipboardTextClockOutline
} from '@mdi/js'
// form
import { mdiCalendarMonth, mdiPlusCircleOutline, mdiMinusCircleOutline } from '@mdi/js'
// Pagination
import { mdiChevronLeft, mdiChevronRight, mdiPageFirst, mdiPageLast } from '@mdi/js'
// Report
import { mdiFormatListBulleted } from '@mdi/js'
// Others
import { mdiMagnify, mdiPlus, mdiArrowLeftBoldCircle } from '@mdi/js'

// All styles for pages
import './scripts/css/customStyle.css'

const vuetify = createVuetify({
  components,
  directives,
  icons: {
    defaultSet: 'mdi',
    aliases: {
      ...aliases,
      // snackbar
      alertCircleOutline: mdiAlertCircleOutline,
      // login
      account: mdiAccount,
      lock: mdiLock,
      eye: mdiEye,
      eyeoff: mdiEyeOff,
      // Dropdown มุมบนขวา + 'mdiAccount, '
      swapHorizontal: mdiSwapHorizontal,
      file: mdiFile,
      power: mdiPower,
      chevronDown: mdiChevronDown,
      // tools + 'mdiEye,
      noteEditOutline: mdiNoteEditOutline,
      trashCanOutline: mdiTrashCanOutline,
      cardAccountDetails: mdiCardAccountDetails,
      // table
      accountOutline: mdiAccountOutline,
      inboxMultipleOutline: mdiInboxMultipleOutline,
      send: mdiSend,
      sendClock: mdiSendClock,
      cursorDefault: mdiCursorDefault,
      clipboardTextClockOutline: mdiClipboardTextClockOutline,
      // form
      calendarMonth: mdiCalendarMonth,
      plusCircleOutline: mdiPlusCircleOutline,
      minusCircleOutline: mdiMinusCircleOutline,
      // Pagination
      chevronLeft: mdiChevronLeft,
      chevronRight: mdiChevronRight,
      pageFirst: mdiPageFirst,
      pageLast: mdiPageLast,
      // Report
      formatListBulleted: mdiFormatListBulleted,
      // Others
      magnify: mdiMagnify,
      plus: mdiPlus,
      arrowLeftBoldCircle: mdiArrowLeftBoldCircle
    },
    sets: {
      mdi
    }
  }
})

const app = createApp(App)

app.use(createPinia())
app.use(router)
app.use(vuetify)

app.mount('#app')
