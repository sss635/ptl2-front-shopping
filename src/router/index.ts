import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      // component: () => import('../views/LoginView.vue'!),
      components: {
        default: () => import('../views/TestFullPageView.vue'),
        header: () => import('@/components/headers/ViewHeader.vue')
      },
      meta: {
        layout: 'FullLayout'
      }
    },
    // {
    //   path: '/:catchAll(.*)*',
    //   name: 'PageNotFound',
    //   component: () => import('../views/PageNotFound.vue'),
    //   meta: {
    //     layout: 'FullLayout'
    //   }
    // },
    {
      path: '/about',
      name: 'about',
      // component: () => import('../views/AboutView.vue')
      components: {
        default: () => import('../views/TestMainPageView.vue'),
        menu: () => import('@/components/headers/MenuTab.vue'),
        header: () => import('@/components/headers/MainHeader.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    }
    // {
    //   path: '/spe-instructor-manage',
    //   name: 'spe-instructor',
    //   components: {
    //     default: () => import('../views/spe_instructor_info/MainSpeInstructor.vue'),
    //     menu: () => import('@/components/headers/MenuTab.vue'),
    //     header: () => import('@/components/headers/MainHeader.vue')
    //   },
    //   meta: {
    //     layout: 'MainLayout',
    //     requiresAuth: true,
    //     requiresURole: [
    //       `${import.meta.env.VITE_ID_ADMIN_ROLE}`,
    //       `${import.meta.env.VITE_ID_OFFC_ROLE}`,
    //       ''
    //     ]
    //   },
    //   children: [
    //     //use with <router-view /> in parent page
    //     {
    //       path: '',
    //       name: 'spe-instructor-manage',
    //       components: {
    //         default: () => import('../views/spe_instructor_info/SpeInstructorManageView.vue'!)
    //       },
    //       meta: {
    //         layout: 'MainLayout',
    //         requiresAuth: true,
    //         requiresURole: [
    //           `${import.meta.env.VITE_ID_ADMIN_ROLE}`,
    //           `${import.meta.env.VITE_ID_OFFC_ROLE}`,
    //           ''
    //         ]
    //       }
    //     },
    //     {
    //       path: 'spe-instructor-form',
    //       name: 'spe-instructor-form',
    //       components: {
    //         default: () => import('../views/spe_instructor_info/SpeInstructorForm.vue'!)
    //       },
    //       meta: {
    //         layout: 'MainLayout',
    //         requiresAuth: true,
    //         requiresURole: [
    //           `${import.meta.env.VITE_ID_ADMIN_ROLE}`,
    //           `${import.meta.env.VITE_ID_OFFC_ROLE}`,
    //           ''
    //         ]
    //       }
    //     },
    //     {
    //       path: 'spe-instructor-viewer/:id',
    //       name: 'spe-instructor-viewer',
    //       components: {
    //         default: () => import('../views/spe_instructor_info/SpeInstructorViewer.vue'!),
    //         header: () => import('@/components/headers/ViewHeader.vue')
    //       },
    //       meta: {
    //         layout: 'FullLayout',
    //         requiresAuth: true,
    //         requiresURole: [
    //           `${import.meta.env.VITE_ID_ADMIN_ROLE}`,
    //           `${import.meta.env.VITE_ID_OFFC_ROLE}`,
    //           ''
    //         ]
    //       }
    //     }
    //   ]
    // },
    // {
    //   path: '/spe-instructor-viewer-email/:id',
    //   name: 'spe-instructor-viewer-email',
    //   components: {
    //     default: () => import('../views/spe_instructor_info/SpeInstructorViewer.vue'!),
    //     header: () => import('@/components/headers/ViewHeader.vue')
    //   },
    //   meta: {
    //     layout: 'FullLayout'
    //   }
    // },
    // {
    //   path: '/propose-to',
    //   name: 'propose-to',
    //   components: {
    //     default: () => import('../views/propose_to/ProposeView.vue'),
    //     menu: () => import('@/components/headers/MenuTab.vue'),
    //     header: () => import('@/components/headers/MainHeader.vue')
    //   },
    //   meta: {
    //     layout: 'MainLayout',
    //     requiresAuth: true,
    //     requiresURole: [
    //       `${import.meta.env.VITE_ID_ADMIN_ROLE}`,
    //       `${import.meta.env.VITE_ID_OFFC_ROLE}`,
    //       ''
    //     ],
    //     requiresChoices: ['propose-to']
    //   }
    // },
    // {
    //   path: '/admin-propose-to',
    //   name: 'admin-propose',
    //   components: {
    //     default: () => import('../views/propose_to/MainAppoint.vue'),
    //     menu: () => import('@/components/headers/MenuTab.vue'),
    //     header: () => import('@/components/headers/MainHeader.vue')
    //   },
    //   meta: {
    //     layout: 'MainLayout',
    //     requiresAuth: true,
    //     requiresURole: [`${import.meta.env.VITE_ID_ADMIN_ROLE}`]
    //   },
    //   children: [
    //     {
    //       path: '',
    //       name: 'admin-propose-to',
    //       components: {
    //         default: () => import('../views/propose_to/AppointAdminView.vue'!)
    //       },
    //       meta: {
    //         layout: 'MainLayout',
    //         requiresAuth: true,
    //         // requiresURole: ["ผู้ดูแลระบบ"],
    //         requiresURole: [`${import.meta.env.VITE_ID_ADMIN_ROLE}`]
    //       }
    //     },
    //     {
    //       path: 'aview-propose-to',
    //       name: 'aview-propose-to',
    //       components: {
    //         default: () => import('../views/propose_to/AppointViewer.vue'!)
    //       },
    //       meta: {
    //         layout: 'MainLayout',
    //         requiresAuth: true,
    //         requiresURole: [
    //           `${import.meta.env.VITE_ID_ADMIN_ROLE}`,
    //           `${import.meta.env.VITE_ID_OFFC_ROLE}`,
    //           ''
    //         ]
    //       }
    //     }
    //   ]
    // },
    // {
    //   path: '/offc-propose-to',
    //   name: 'offc-propose',
    //   components: {
    //     default: () => import('../views/propose_to/MainAppoint.vue'),
    //     menu: () => import('@/components/headers/MenuTab.vue'),
    //     header: () => import('@/components/headers/MainHeader.vue')
    //   },
    //   meta: {
    //     layout: 'MainLayout',
    //     requiresAuth: true,
    //     requiresURole: [`${import.meta.env.VITE_ID_OFFC_ROLE}`]
    //   },
    //   children: [
    //     {
    //       path: '',
    //       name: 'offc-propose-to',
    //       components: {
    //         default: () => import('../views/propose_to/AppointOfficerView.vue'!)
    //       },
    //       meta: {
    //         layout: 'MainLayout',
    //         requiresAuth: true,
    //         requiresURole: [`${import.meta.env.VITE_ID_OFFC_ROLE}`]
    //       }
    //     },
    //     {
    //       path: 'oview-propose-to',
    //       name: 'oview-propose-to',
    //       components: {
    //         default: () => import('../views/propose_to/AppointViewer.vue'!)
    //       },
    //       meta: {
    //         layout: 'MainLayout',
    //         requiresAuth: true,
    //         requiresURole: [
    //           `${import.meta.env.VITE_ID_ADMIN_ROLE}`,
    //           `${import.meta.env.VITE_ID_OFFC_ROLE}`,
    //           ''
    //         ]
    //       }
    //     },
    //     {
    //       path: 'appoint-document',
    //       name: 'appoint-document',
    //       components: {
    //         default: () => import('../views/propose_to/AppointDocument.vue'!)
    //       },
    //       meta: {
    //         layout: 'MainLayout',
    //         requiresAuth: true,
    //         requiresURole: [
    //           `${import.meta.env.VITE_ID_ADMIN_ROLE}`,
    //           `${import.meta.env.VITE_ID_OFFC_ROLE}`,
    //           ''
    //         ]
    //       }
    //     },
    //     {
    //       path: 'add-appoint-propose',
    //       name: 'appoint-propose',
    //       components: {
    //         default: () => import('../views/propose_to/MainAppoint.vue'),
    //         menu: () => import('@/components/headers/MenuTab.vue'),
    //         header: () => import('@/components/headers/MainHeader.vue')
    //       },
    //       meta: {
    //         layout: 'MainLayout',
    //         requiresAuth: true,
    //         requiresURole: [
    //           `${import.meta.env.VITE_ID_ADMIN_ROLE}`,
    //           `${import.meta.env.VITE_ID_OFFC_ROLE}`,
    //           ''
    //         ]
    //       },
    //       children: [
    //         {
    //           path: '',
    //           name: 'add-appoint-propose',
    //           components: {
    //             default: () => import('../views/propose_to/AppointPropose.vue'!)
    //           },
    //           meta: {
    //             layout: 'MainLayout',
    //             requiresAuth: true,
    //             requiresURole: [
    //               `${import.meta.env.VITE_ID_ADMIN_ROLE}`,
    //               `${import.meta.env.VITE_ID_OFFC_ROLE}`,
    //               ''
    //             ]
    //           }
    //         },
    //         {
    //           path: 'add-appoint-lect',
    //           name: 'add-appoint-lect',
    //           components: {
    //             default: () => import('../views/propose_to/AppointLectForm.vue'!)
    //           },
    //           meta: {
    //             layout: 'MainLayout',
    //             requiresAuth: true,
    //             requiresURole: [
    //               `${import.meta.env.VITE_ID_ADMIN_ROLE}`,
    //               `${import.meta.env.VITE_ID_OFFC_ROLE}`,
    //               ''
    //             ]
    //           }
    //         },
    //         {
    //           path: 'view-appoint-lect',
    //           name: 'view-appoint-lect',
    //           components: {
    //             default: () => import('../views/propose_to/AppointLectView.vue'!)
    //           },
    //           meta: {
    //             layout: 'MainLayout',
    //             requiresAuth: true,
    //             requiresURole: [
    //               `${import.meta.env.VITE_ID_ADMIN_ROLE}`,
    //               `${import.meta.env.VITE_ID_OFFC_ROLE}`,
    //               ''
    //             ]
    //           }
    //         }
    //       ]
    //     },
    //     {
    //       path: 'create-account',
    //       name: 'create-account',
    //       components: {
    //         default: () => import('../views/manage_user/CreateAccountView.vue'!)
    //       },
    //       meta: {
    //         layout: 'MainLayout',
    //         requiresAuth: true,
    //         requiresURole: [
    //           `${import.meta.env.VITE_ID_ADMIN_ROLE}`,
    //           `${import.meta.env.VITE_ID_OFFC_ROLE}`,
    //           ''
    //         ]
    //       }
    //     },
    //   ]
    // },
    // {
    //   path: '/manage-year-semester',
    //   name: 'manage-year-semester',
    //   components: {
    //     default: () => import('../views/manage_year_semester/ManageYearSmsView.vue'!),
    //     menu: () => import('@/components/headers/MenuTab.vue'),
    //     header: () => import('@/components/headers/MainHeader.vue')
    //   },
    //   meta: {
    //     layout: 'MainLayout',
    //     requiresAuth: true,
    //     requiresURole: [`${import.meta.env.VITE_ID_ADMIN_ROLE}`]
    //   }
    // },
    // {
    //   path: '/manage-user',
    //   name: 'manage-user',
    //   components: {
    //     default: () => import('../views/propose_to/MainAppoint.vue'),
    //     menu: () => import('@/components/headers/MenuTab.vue'),
    //     header: () => import('@/components/headers/MainHeader.vue')
    //   },
    //   meta: {
    //     layout: 'MainLayout',
    //     requiresAuth: true,
    //     requiresURole: [`${import.meta.env.VITE_ID_ADMIN_ROLE}`]
    //   },
    //   children: [
    //     {
    //       path: '',
    //       name: 'manage-user-to',
    //       components: {
    //         default: () => import('../views/manage_user/ManageUserView.vue'!)
    //       },
    //       meta: {
    //         layout: 'MainLayout',
    //         requiresAuth: true,
    //         requiresURole: [`${import.meta.env.VITE_ID_ADMIN_ROLE}`]
    //       }
    //     },
    //     {
    //       path: 'add-per-user',
    //       name: 'add-per-user',
    //       components: {
    //         default: () => import('../views/manage_user/AddPerUserView.vue'!)
    //       },
    //       meta: {
    //         layout: 'MainLayout',
    //         requiresAuth: true,
    //         requiresURole: [`${import.meta.env.VITE_ID_ADMIN_ROLE}`]
    //       }
    //     }
    //   ]
    // },
    // // {
    // //   path: '/manual',
    // //   name: 'manual',
    // //   components: {
    // //     default: () => import('../views/manual/ManualTools.vue'!),
    // //     menu: () => import('@/components/headers/MenuTab.vue'),
    // //     header: () => import('@/components/headers/MainHeader.vue')
    // //   },
    // //   meta: {
    // //     layout: 'MainLayout',
    // //     requiresAuth: true,
    // //     requiresURole: [
    // //       `${import.meta.env.VITE_ID_ADMIN_ROLE}`,
    // //       `${import.meta.env.VITE_ID_OFFC_ROLE}`,
    // //       ''
    // //     ]
    // //   }
    // // },
    // {
    //   path: '/report',
    //   name: 'report',
    //   components: {
    //     default: () => import('../views/report/MainReport.vue'),
    //     menu: () => import('@/components/headers/MenuTab.vue'),
    //     header: () => import('@/components/headers/MainHeader.vue')
    //   },
    //   meta: {
    //     layout: 'MainLayout',
    //     requiresAuth: true,
    //     requiresURole: [`${import.meta.env.VITE_ID_ADMIN_ROLE}`]
    //   },
    //   children: [
    //     // {
    //     //   path: '',
    //     //   name: '..',
    //     //   components: {
    //     //     default: () => import('../views/report/...vue'!)
    //     //   },
    //     //   meta: {
    //     //     layout: 'MainLayout',
    //     //     requiresAuth: true,
    //     //     requiresURole: [`${import.meta.env.VITE_ID_ADMIN_ROLE}`]
    //     //   }
    //     // },
    //     {
    //       path: 'rp-register',
    //       name: 'rp-register',
    //       components: {
    //         default: () => import('../views/report/RegisterRPView.vue'!)
    //       },
    //       meta: {
    //         layout: 'MainLayout',
    //         requiresAuth: true,
    //         requiresURole: [`${import.meta.env.VITE_ID_ADMIN_ROLE}`]
    //       }
    //     },
    // ]
    // },
  ]
})

const isLogin = () => {
  const user = localStorage.getItem('username')
  if (user) {
    return true
  }
  return false
}

router.beforeEach((to, from) => {
  // instead of having to check every route record with
  if (to.meta.requiresAuth && !isLogin()) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    return {
      path: '/',
      // path: "/login",
      // save the location we were at to come back later
      query: { redirect: to.fullPath }
    }
  } else if (to.meta.requiresAuth && isLogin()) {
    const sRole = localStorage.getItem('selectRole')!
    const admin = import.meta.env.VITE_ID_ADMIN_ROLE
    const offc = import.meta.env.VITE_ID_OFFC_ROLE

    if (`[${to.meta.requiresChoices}]`.includes('propose-to')) {
      if (sRole.includes(admin)) {
        return {
          path: '/admin-propose-to'
        }
      } else if (sRole.includes(offc)) {
        return {
          path: '/offc-propose-to'
        }
      }
    }
    if (to.meta.requiresURole && !`[${to.meta.requiresURole}]`.includes(sRole)) {
      return {
        path: '/propose-to'
      }
    }
  }
})

export default router
